\version  "2.18.2"



%#(set-global-staff-size 17)
\paper {
  indent = 2.5\cm % space for instrumentName
  short-indent = 1\cm % space for shortInstrumentName
}
cresc = \markup{\italic "cresc."}

  % Pitches as written on a manuscript for Clarinet in A
  % are transposed to concert pitch
  hautbois_un_Music = \relative a'' { \key d \minor 
                                   \tempo "Andante"
                                   \partial 4 a4 \p |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d, |
                                      e4. a,8 a4 e'4--\( |
                                      a,4--\) r4 r a'4 |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d,_\markup{\italic "dimin."}|
                                      \acciaccatura ees8 d4 bes8-.\( c-.\) d4 r |

                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2 r4 \fermata
                                      a'4 \p |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d, |
                                      e4. a,8 a4 e'4--\( |
                                      a,4--\) r4 r a'4 |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d,_\markup{\italic "dimin."} |
                                      \acciaccatura ees8 d4 bes8-.\( c-.\) d4 r |
         
                                 
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2 r4 \fermata 
                                  \bar "||"
                                  \key d \major 
                                  r4^\markup{ \bold "Allegro ma non troppo"} |
                                  r2 r4
                                      \tuplet 3/2 {fis8-. \p fis-. fis-.} |
                                      fis2._\cresc
                                   
                                  r4 |
                                  r2 r4 
                                    \new Voice \voiceOne {
                                      \tuplet 3/2 {fis8-. g-. a-.} | 
                                      e2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis2.-> \tuplet 3/2 {fis8-. e-. fis-.} |
                                      g2.-> \tuplet 3/2 {gis8-. \f gis-. gis-.} |
                                      gis2.-> \tuplet 3/2 {gis8-. fis-. gis-.} |
                                      a2.-> \tuplet 3/2 {fis8-._\cresc fis-. fis-.} |
                                      fis2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis4 \tuplet 3/2 {ais8-. ais-. ais-.} ais4 \tuplet 3/2 {b8-. b-. b-.} |
                                      b2. \ff \tuplet 3/2 {a8-. a-. a-.} |
                                      a2. \tuplet 3/2 {a8-. \< a-. a-.} |
                                      a4 \tuplet 3/2 {g8-. g-. g-.} g4 \tuplet 3/2 {a8-. a-. a-.} |
                                      a4 \! \tuplet 3/2 {fis8-. fis-. fis-.} fis4  \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis4 \tuplet 3/2 {fis8-. fis-. fis-.} fis4  \tuplet 3/2 {f8-. f-. f-.} |
                                      f4 \tuplet 3/2 {d8-. d-. d-.} d4  \tuplet 3/2 {f8-. f-. f-.} |
                                      f4 \tuplet 3/2 {d8-. d-. d-.} d4  f4 \ff |
                                    }
                                   
                                  \key d \minor
                                    ees4_"pesante" f8 \( ees \) c4 bes |
                                    f' ees c4. bes8 |
                                    bes4  g'-> f-> ees8 \( f \) |
                                    d4 c4 g' f |
                                    d4. c8 c4 
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1^"Poco a poco riten." |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. 
                                      a'4^\markup{ \bold "Tempo I"} \p |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d, |
                                      e4. a,8 a4 e'4--\( |
                                      a,4--\) r4 r a'4 \p |
                                      \acciaccatura bes8 a4 f8-.\( g-.\) a4 d, |
                                      \acciaccatura f8 e4 c8-.\( d-.\) e4 
                                
                                  r4 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2. \fermata \bar "|."
                                }

  hautbois_deux_Music = \relative a'' { \key d \minor 
                                      \partial 4 f4 \p |
                                      \acciaccatura g8 f4 d8-. \( e-. \) f4 f, |
                                      a4. a8 a4 e'4-- \( |
                                      a,4-- \) r4 r f'4 |
                                      \acciaccatura g8 f4 d8-. \( e-. \) f4 bes,_\markup{\italic "dimin."}	 |
                                      \acciaccatura c8 bes4 g8-. \( a-. \) bes4 r |
                                   
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2 r4 \fermata
                                 
                                      f'4 \p |
                                      \acciaccatura g8 f4 d8-. \( e-. \) f4 f, |
                                      a4. a8 a4 e'4-- \( |
                                      a,4-- \) r4 r f'4 |
                                      \acciaccatura g8 f4 d8-. \( e-. \) f4 bes,_\markup{\italic "dimin."}	 |
                                      \acciaccatura c8 bes4 g8-. \( a-. \) bes4 r |

                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2 r4 \fermata 
                                  \bar "||"
                                  \key d \major 
                                  r4 |
                                  r2 r4

                                      \tuplet 3/2 {d8-. \p d-. d-.} |
                                      d2._\markup{ \italic "cresc."}

                                  r4 |
                                  r2 r4 


                                      \tuplet 3/2 {d8-. e-. fis-.} |
                                      a,2. \tuplet 3/2 {d8-. d-. d-.} |
                                      d2.-> \tuplet 3/2 {dis8-. cis-. b-.} |
                                      b2.-> \tuplet 3/2 {b8-. \f b-. b-.} |
                                      b2.-> \tuplet 3/2 {eis8-. dis-. cis-.} |
                                      cis2.-> \tuplet 3/2 {ais8-._\markup{ \italic "cresc."} ais-. ais-.} |
                                      ais2. \tuplet 3/2 {b8-. b-. b-.} |
                                      b4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                      dis2. \ff \tuplet 3/2 {e8-. e-. e-.} |
                                      e2. \tuplet 3/2 {fis8-. \< fis-. fis-.} |
                                      fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {fis8-.  fis-. fis-.} |
                                      fis4 \! \tuplet 3/2 {a,8-. a-. a-.} a4  \tuplet 3/2 {d8-. d-. d-.} |
                                      d4 \tuplet 3/2 {a8-. a-. a-.} a4  \tuplet 3/2 {d8-. d-. d-.} |
                                      d4 \tuplet 3/2 {f,8-. f-. f-.} f4  \tuplet 3/2 {d'8-. d-. d-.} |
                                      d4 \tuplet 3/2 {f,8-. f-. f-.} f4  d'4 \ff |

                                  \key d \minor
                                 
                                    c4_"pesante" \! bes8 \( c8 \) a4 g |
                                    d' c a4. g8 |
                                    g4 ees' d bes8 \( d \) |
                                    b4 g4 ees' d |
                                    b4. g8 g4 

                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. 

                                      f'4 |
                                      \acciaccatura g8 f4 d8-. \(  e-. \) f4 f, |
                                      a4. a8 a4 e'4-- \( |
                                      a,4-- \) r4 r f'4 \p |
                                      \acciaccatura g8 f4 d8-. \( e-. \) f4 f, |
                                      \acciaccatura d'8 c4 a8-. \( b-. \) c4 

                                  r4 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2. \fermata \bar "|."
                                }
                                
  

\header	{
  title = "Ballade pour piano"
  %subtitle = "Fautbois"
  composer = "J. Brahms, Op. 10, n°1"
  arranger = "arr : L. Feisthauer"
}

\score {
    <<
      \new GrandStaff = "StaffGroup_hautbois" <<
        
        \new Staff = "Staff_hautbois" \with {
          instrumentName = #"Hautbois 1"
          shortInstrumentName = #"Hb. 1"
        }
        {
          \hautbois_un_Music
        }
        \new Staff = "Staff_hautbois" \with {
          instrumentName = #"Hautbois 2"
          shortInstrumentName = #"Hb. 2"
        }
        {
         
          \hautbois_deux_Music
        }
       
        
      >>
      
        
     
      
>>
}

\layout {

  }
