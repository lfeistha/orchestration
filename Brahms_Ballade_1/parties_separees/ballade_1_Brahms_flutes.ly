\version  "2.18.2"



%#(set-global-staff-size 17)
\paper {
  indent = 2.5\cm % space for instrumentName
  short-indent = 1\cm % space for shortInstrumentName
}
cresc = \markup{\italic "cresc."}
 
 flute_un_music = \relative a'' { \key d \minor
                               \tempo "Andante"
                               \partial 4 r4 |
                                R1 |
                                R1 |
                                r2 r4  a4 \p  |
                                \acciaccatura bes8 a4 f8-.\( g-.\) a4 r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata r4 |
                                R1 |
                                R1 |
                                r2 r4 a4 \p  |
                                \acciaccatura bes8 a4 f8-.\( g-.\) a4 r4 |
                                
                                R1 |
                                R1 |
                                R1 |
                                r2. r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata
                                \bar "||"
                                \key d \major 
                                r4^\markup{ \bold "Allegro ma non troppo"} |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 
                                    \tuplet 3/2 {fis'8-. \f fis-._\cresc fis-.} |
                                    fis2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                    fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                    dis2. \tuplet 3/2 {e8-. e-. e-.} |
                                    e2. \tuplet 3/2 {fis8-. \< fis-. fis-.} |
                                    fis4 \tuplet 3/2 {g8-. g-. g-.} fis4 \tuplet 3/2 {a8-. a-. a-.} |
                                    a4 \! \tuplet 3/2 {a,8-. a-. a-.} a4 \tuplet 3/2 {fis'8-. fis-. fis-.} |
                                    fis4 \tuplet 3/2 {a,8-. a-. a-.} a4 \tuplet 3/2 {f'8-. f-. f-.} |
                                    f4 \tuplet 3/2 {f,8-. f-. f-.} f4 \tuplet 3/2 {f'8-. f-. f-.} |
                                    f4 \tuplet 3/2 {f,8-. f-. f-.} f4 d' \ff | 
                                 
                                \key d \minor
                               
                                    c4_"pesante" \! bes8 \( c \) a4 g |
                                    d' c a4. g8 |
                                    g4 
                                 
                                r4 r2 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1^"Poco a poco riten." |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. r4^\markup{ \bold "Tempo I"} |
                                R1 |
                                R1 |
                                r2 r4
                                
                                      a4 \p  |
                                     \acciaccatura bes8 a4 f8--\( g--\) a4 
                               
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. \fermata \bar "|."
                             } 

flute_deux_music = \relative a'' { \key d \minor
                               \tempo "Andante"
                               \partial 4 r4 |
                                R1 |
                                R1 |
                                r2 r4
                                     f4 \p |
                                   \acciaccatura g8 f4 d8 e f4 r4 |
                              	
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata r4 |
                                R1 |
                                R1 |
                                r2 r4
                                    f4 \p |
                                   \acciaccatura g8 f4 d8 e f4 r4 |
                               
                                R1 |
                                R1 |
                                R1 |
                                r2. r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata
                                \bar "||"
                                \key d \major 
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 
                                    \tuplet 3/2 {ais8-. \f ais-._\markup{\italic "cresc."}  ais-.} |
                                    ais2. \tuplet 3/2 {b8-. b-. b-.} |
                                    b4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                    b2. \tuplet 3/2 {cis8-. cis-. cis-.} |
                                    cis2. \tuplet 3/2 {d8-. \< d-. d-.} |
                                    d4 \tuplet 3/2 {d8-. d-. d-.} d4 \tuplet 3/2 {d8-. d-. d-.} |
                                    d4 \! \tuplet 3/2 {fis,8-. fis-. fis-.} fis4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {fis,8-. fis-. fis-.} fis4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {d,8-. d-. d-.} d4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {d,8-. d-. d-.} d4 bes' \ff  | 
                                  
                                
                                \key d \minor
                              
                    
                                    a4_"pesante" \! f8 a8 fis4 d |
                                    bes' a fis4. d8 |
                                    d4
                               
                                r4 r2 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1|
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. r4|
                                R1 |
                                R1 |
                                r2 r4
                                     f4 \p |
                                   \acciaccatura g8 f4 d8 e f4 
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. \fermata \bar "|."
                             } 
  

\header	{
  title = "Ballade pour piano"
  subtitle = "Flûtes 1 & 2"
  composer = "J. Brahms, Op. 10, n°1"
  arranger = "arr : L. Feisthauer"
}

\score {
        \new GrandStaff = "GrandStaff_flutes" <<
          \new Staff = "Staff_flute1" \with{
            instrumentName = #"Flûte I"
            shortInstrumentName = #"Flt I"
          }
          {
            \flute_un_music
          }
          \new Staff = "Staff_fluteII" \with{
            instrumentName = #"Flûte II"
            shortInstrumentName = #"Flt II"
          }
          {
            \flute_deux_music
          }
        >>
       

\layout {
   #(layout-set-staff-size 15)

  }
}