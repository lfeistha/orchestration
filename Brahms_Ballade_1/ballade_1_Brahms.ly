\version  "2.18.2"



%#(set-global-staff-size 17)
\paper {
  indent = 2.5\cm % space for instrumentName
  short-indent = 1\cm % space for shortInstrumentName
}
cresc = \markup{\italic "cresc."}
  fluteMusic = \relative a'' { \key d \minor
                               \tempo "Andante"
                               \partial 4 r4 |
                                R1 |
                                R1 |
                                r2 r4
                                << 
                                  \new Voice {
                                     \voiceOne 
                                      a4 \p  |
                                     \acciaccatura bes8 a4 f8-.\( g-.\) a4 r4 |
                                    }
                                \\
                                  \new Voice {
                                    \voiceTwo 
                                     f4 |
                                   \acciaccatura g8 f4 d8 e f4 r4 |
                                  }
                                >>	
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata r4 |
                                R1 |
                                R1 |
                                r2 r4
                                << 
                                  \new Voice {
                                     \voiceOne 
                                     a4 \p  |
                                     \acciaccatura bes8 a4 f8-.\( g-.\) a4 r4 |
                                    }
                                \\
                                  \new Voice {
                                    \voiceTwo 
                                    f4 |
                                   \acciaccatura g8 f4 d8 e f4 r4 |
                                  }
                                >>	
                                R1 |
                                R1 |
                                R1 |
                                r2. r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 \fermata
                                \bar "||"
                                \key d \major 
                                r4^\markup{ \bold "Allegro ma non troppo"} |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2 r4 
                                <<
                                  \new Voice \voiceOne {
                                    \tuplet 3/2 {fis'8-. fis-. fis-.} |
                                    fis2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                    fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                    dis2. \tuplet 3/2 {e8-. e-. e-.} |
                                    e2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                    fis4 \tuplet 3/2 {g8-. g-. g-.} fis4 \tuplet 3/2 {a8-. a-. a-.} |
                                    a4 \tuplet 3/2 {a,8-. a-. a-.} a4 \tuplet 3/2 {fis'8-. fis-. fis-.} |
                                    fis4 \tuplet 3/2 {a,8-. a-. a-.} a4 \tuplet 3/2 {f'8-. f-. f-.} |
                                    f4 \tuplet 3/2 {f,8-. f-. f-.} f4 \tuplet 3/2 {f'8-. f-. f-.} |
                                    f4 \tuplet 3/2 {f,8-. f-. f-.} f4 d' | 
                                  }
                                  \\
                                  \new Voice \voiceTwo {
                                    \tuplet 3/2 {ais8-. \f ais-._\markup{\italic "cresc."}  ais-.} |
                                    ais2. \tuplet 3/2 {b8-. b-. b-.} |
                                    b4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                    b2. \tuplet 3/2 {cis8-. cis-. cis-.} |
                                    cis2. \tuplet 3/2 {d8-. \< d-. d-.} |
                                    d4 \tuplet 3/2 {d8-. d-. d-.} d4 \tuplet 3/2 {d8-. d-. d-.} |
                                    d4 \! \tuplet 3/2 {fis,8-. fis-. fis-.} fis4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {fis,8-. fis-. fis-.} fis4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {d,8-. d-. d-.} d4 \tuplet 3/2 {d'8-. d-. d-.} |
                                    d4 \tuplet 3/2 {d,8-. d-. d-.} d4 bes' \ff  | 
                                  }
                                >>
                                \key d \minor
                                <<
                                  \new Voice \voiceOne {
                                    c4 bes8 \( c \) a4 g |
                                    d' c a4. g8 |
                                    g4 
                                  }
                                  \\
                                  \new Voice \voiceTwo {
                                    a4_"pesante" \! f8 a8 fis4 d |
                                    bes' a fis4. d8 |
                                    d4
                                  }
                                >>
                                r4 r2 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1^"Poco a poco riten." |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. r4^\markup{ \bold "Tempo I"} |
                                R1 |
                                R1 |
                                r2 r4
                                << 
                                  \new Voice {
                                     \voiceOne 
                                      a4 \p  |
                                     \acciaccatura bes8 a4 f8--\( g--\) a4 
                                    }
                                \\
                                  \new Voice {
                                    \voiceTwo 
                                     f4 |
                                   \acciaccatura g8 f4 d8 e f4 
                                  }
                                >>
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. \fermata \bar "|."
                             } 
  % Pitches as written on a manuscript for Clarinet in A
  % are transposed to concert pitch
  hautboisMusic = \relative a'' { \key d \minor 
                                  <<
                                    \new Voice \voiceOne {
                                      \partial 4 a4 \p |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      e4. a,8 a4 e'4--\( |
                                      a,4--\) r4 r a'4 |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      \acciaccatura ees8 d4 bes8--\( c--\) d4 r |
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      \partial 4 f4 |
                                      \acciaccatura g8 f4 d8 e f4 f, |
                                      a4. a8 a4 e'4 |
                                      a,4 r4 r f'4 |
                                      \acciaccatura g8 f4 d8 e f4 bes,_\markup{\italic "dimin."}	 |
                                      \acciaccatura c8 bes4 g8 a bes4 r |
                                    }
                                  >>
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r2 r4 \fermata
                                  <<
                                    \new Voice \voiceOne {
                                      a'4 \p |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      e4. a,8 a4 e'4--\( |
                                      a,4--\) r4 r a'4 |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      \acciaccatura ees8 d4 bes8--\( c--\) d4 r |
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      f4 |
                                      \acciaccatura g8 f4 d8 e f4 f, |
                                      a4. a8 a4 e'4 |
                                      a,4 r4 r f'4 |
                                      \acciaccatura g8 f4 d8 e f4 bes,_\markup{\italic "dimin."}	 |
                                      \acciaccatura c8 bes4 g8 a bes4 r |
                                    }
                                  >>
                                  r1 |
                                  r1 |
                                  r2. r4 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r2 r4 \fermata 
                                  \bar "||"
                                  \key d \major 
                                  r4 |
                                  r2 r4
                                  <<
                                    \new Voice \voiceOne {
                                      \tuplet 3/2 {fis'8-. fis-. fis-.} |
                                      fis2.
                                    }
                                    \\
                                    \new Voice \voiceTwo {
                                      \tuplet 3/2 {d8-. \p d-. d-.} |
                                      d2._\markup{ \italic "cresc."}
                                    }
                                  >>
                                  r4 |
                                  r2 r4 
                                  <<
                                    \new Voice \voiceOne {
                                      \tuplet 3/2 {fis8-. g-. a-.} | 
                                      e2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis2.-> \tuplet 3/2 {fis8-. e-. fis-.} |
                                      g2.-> \tuplet 3/2 {gis8-. gis-. gis-.} |
                                      gis2.-> \tuplet 3/2 {gis8-. fis-. gis-.} |
                                      a2.-> \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis4 \tuplet 3/2 {ais8-. ais-. ais-.} ais4 \tuplet 3/2 {b8-. b-. b-.} |
                                      b2. \tuplet 3/2 {a8-. a-. a-.} |
                                      a2. \tuplet 3/2 {a8-. a-. a-.} |
                                      a4 \tuplet 3/2 {g8-. g-. g-.} g4 \tuplet 3/2 {a8-. a-. a-.} |
                                      a4 \tuplet 3/2 {fis8-. fis-. fis-.} fis4  \tuplet 3/2 {fis8-. fis-. fis-.} |
                                      fis4 \tuplet 3/2 {fis8-. fis-. fis-.} fis4  \tuplet 3/2 {f8-. f-. f-.} |
                                      f4 \tuplet 3/2 {d8-. d-. d-.} d4  \tuplet 3/2 {f8-. f-. f-.} |
                                      f4 \tuplet 3/2 {d8-. d-. d-.} d4  f4 |
                                    }
                                    \\
                                    \new Voice \voiceTwo {
                                      \tuplet 3/2 {d8-. e-. fis-.} |
                                      a,2. \tuplet 3/2 {d8-. d-. d-.} |
                                      d2.-> \tuplet 3/2 {dis8-. cis-. b-.} |
                                      b2.-> \tuplet 3/2 {b8-. \f b-. b-.} |
                                      b2.-> \tuplet 3/2 {eis8-. dis-. cis-.} |
                                      cis2.-> \tuplet 3/2 {ais8-._\markup{ \italic "cresc."} ais-. ais-.} |
                                      ais2. \tuplet 3/2 {b8-. b-. b-.} |
                                      b4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                      dis2. \ff \tuplet 3/2 {e8-. e-. e-.} |
                                      e2. \tuplet 3/2 {fis8-. \< fis-. fis-.} |
                                      fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {fis8-.  fis-. fis-.} |
                                      fis4 \! \tuplet 3/2 {a,8-. a-. a-.} a4  \tuplet 3/2 {d8-. d-. d-.} |
                                      d4 \tuplet 3/2 {a8-. a-. a-.} a4  \tuplet 3/2 {d8-. d-. d-.} |
                                      d4 \tuplet 3/2 {f,8-. f-. f-.} f4  \tuplet 3/2 {d'8-. d-. d-.} |
                                      d4 \tuplet 3/2 {f,8-. f-. f-.} f4  d'4 \ff |
                                    }
                                  >>
                                  \key d \minor
                                  <<
                                  \new Voice \voiceOne {
                                    ees4 f8 \( ees \) c4 bes |
                                    f' ees c4. bes8 |
                                    bes4  g'-> f-> ees8 \( f \) |
                                    d4 c4 g' f |
                                    d4. c8 c4 
                                  }
                                  \\
                                  \new Voice \voiceTwo {
                                    c4_"pesante" \! bes8 c8 a4 g |
                                    d' c a4. g8 |
                                    g4 ees' d bes8 \( d \) |
                                    b4 g4 ees' d |
                                    b4. g8 g4 
                                  }
                                >>
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. 
                                <<
                                    \new Voice \voiceOne {
                                      a'4 \p |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      e4.-- a,8-- a4 e'4\( |
                                      a,4\) r4 r a'4 |
                                      \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                      \acciaccatura f8 e4 c8--\( d--\) e4 
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      f4 |
                                      \acciaccatura g8 f4 d8 e f4 f, |
                                      a4. a8 a4 e'4 |
                                      a,4 r4 r f'4 \p |
                                      \acciaccatura g8 f4 d8 e f4 f, |
                                      \acciaccatura d'8 c4 a8 b c4 
                                    }
                                  >>
                                  r4 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  R1 |
                                  r2. \fermata \bar "|."
                                }
                                
                                
  clarinetMusic = \transpose c' bes
                  \relative c'' { \key e \minor
                                  <<
                                    \new Voice \voiceOne {
                                      \partial 4 b4 \p |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      fis4. b,8 b4 fis'4--\( |
                                      b,4--\) r4 r b'4 |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      \acciaccatura f8 e4 c8--\( d--\) e4 
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      \partial 4 g4 |
                                      \acciaccatura a8 g4 e8 fes g4 g, |
                                      b4. b8 b4 fes'4 |
                                      b,4 r4 r g'4 |
                                      \acciaccatura a8 g4 e8 fes g4 c,_\markup{\italic "dimin."}	 |
                                      \acciaccatura d8 c4 a8 b c4 
                                    }
                                  >>
                                  a'\(\>^"à 2" |
                                  dis,2.\) dis4\( |
                                  e2.\) ais4\(  |
                                  b2.\) \pp r4 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r2 r4 \fermata
                                  <<
                                    \new Voice \voiceOne {
                                      b4 \p |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      fis4. b,8 b4 fis'4--\( |
                                      b,4--\) r4 r b'4 |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      \acciaccatura f8 e4 c8--\( d--\) e4 
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      g4 |
                                      \acciaccatura a8 g4 e8 fes g4 g, |
                                      b4. b8 b4 fes'4 |
                                      b,4 r4 r g'4 |
                                      \acciaccatura a8 g4 e8 fes g4 c,_\markup{\italic "dimin."}	 |
                                      \acciaccatura d8 c4 a8 b c4 
                                    }
                                  >>
                                  a'\(\>^"à 2" |
                                  dis,2. \) dis4\( |
                                  e2.\) c4\( |
                                  g2.\) \pp r4|
                                  r1 |
                                  r1 |
                                  r1 |
                                  r1 |
                                  r2 r4 \fermata
                                  \bar "||"
                                  \key e \major
                                  <<
                                    \new Voice \voiceOne {
                                      b4^\markup{"1 ."}~ \p |
                                      b2. r4 |
                                      r2. b4~ |
                                      b2. r4 |
                                      r2 .
                                    }
                                  \\
                                    \new Voice \voiceTwo {
                                      s4 |
                                      r2. b4_\markup{"2."}~ |
                                      b2._\cresc r4 |
                                      r2. b4~  |
                                      b2. 
                                    }
                                  >>
                                  <<
                                    \new Voice \voiceOne {
                                       \tuplet 3/2 {gis'8-. gis-. gis-.} |
                                      gis2.-> \tuplet 3/2 {gis8-. fis-. gis-.} |
                                      a2.-> \tuplet 3/2 {ais8-. ais-. ais-.} |
                                      ais2.-> \tuplet 3/2 {ais8-. gis-. ais-.} |
                                      b2.-> \tuplet 3/2 {bis8-. bis-. bis-.} |
                                      bis2. \tuplet 3/2 {cis8-. cis-. cis-.} |
                                      cis4 \tuplet 3/2 {dis8-. dis-. dis-.} dis4 \tuplet 3/2 {eis8-. eis-. eis-.} |
                                      eis2. \tuplet 3/2 {dis8-. dis-. dis-.} |
                                      dis2. \tuplet 3/2 {gis8-. gis-. gis-.} |
                                      gis4 \tuplet 3/2 {fis8-. fis-. fis-.} fis4 \tuplet 3/2 {gis8-. gis-. gis-.} |
                                      gis4 \tuplet 3/2 {b,8-. b-. b-.} b4  \tuplet 3/2 {e8-. e-. e-.} |
                                      e4 \tuplet 3/2 {b8-. b-. b-.} b4  \tuplet 3/2 {e8-. e-. e-.} |
                                      e4 \tuplet 3/2 {g,8-. g-. g-.} g4  \tuplet 3/2 {e'8-. e-. e-.} |
                                      e4 \tuplet 3/2 {g,8-. g-. g-.} g4  e'4 |
                                    }
                                    \\
                                    \new Voice \voiceTwo {
                                       \tuplet 3/2 {e,8-. e-. e-.} |
                                      e2.-> \tuplet 3/2 {eis8-. dis-. cis-.} |
                                      cis2.-> \tuplet 3/2 {cis8-. \f cis-. cis-.} |
                                      cis2.-> \tuplet 3/2 {fisis8-. eis-. dis-.} |
                                      dis2.-> \tuplet 3/2 {gis8-._\markup{ \italic "cresc."} gis-. gis-.} |
                                      gis2. \tuplet 3/2 {gis8-. gis-. gis-.} |
                                      gis4 \tuplet 3/2 {bis8-. bis-. bis-.} bis4 \tuplet 3/2 {cis8-. cis-. cis-.} |
                                      cis2. \ff \tuplet 3/2 {b8-. b-. b-.} |
                                      b2. \tuplet 3/2 {b8-. \< b-. b-.} |
                                      b4 \tuplet 3/2 {a8-. a-. a-.} a4 \tuplet 3/2 {b8-.  b-. b-.} |
                                      b4 \! \tuplet 3/2 {gis8-. gis-. gis-.} gis4  \tuplet 3/2 {gis8-. gis-. gis-.} |
                                      gis4 \tuplet 3/2 {gis8-. gis-. gis-.} gis4  \tuplet 3/2 {g8-. g-. g-.} |
                                      g4 \tuplet 3/2 {e8-. e-. e-.} e4  \tuplet 3/2 {g8-. g-. g-.} |
                                      g4 \tuplet 3/2 {e8-. e-. e-.} e4  c'4 \ff |
                                    }
                                  >>
                                  \key e \minor
                                  <<
                                  \new Voice \voiceOne {
                                    d4 c8 \( d \) b4 a |
                                    e' d b4. a8 |
                                    a4  c-> bes-> c8 \( bes \) |
                                    g4 f4 c' bes |
                                    g4. f8 f4 
                                  }
                                  \\
                                  \new Voice \voiceTwo {
                                    b4_"pesante" \! g8 b8 gis4 a |
                                    c b gis4. a8 |
                                    a4 a g f8 \( g \) |
                                    e4 d4 a' g |
                                    e4. d8 d4 
                                  }
                                >>
                                r4 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                R1 |
                                r2. 
                                <<
                                    \new Voice \voiceOne {
                                      b'4  |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      fis4.-- b,8-- b4 fis'4\( |
                                      b,4\) \tuplet 3/2 {r8-. g'-. fis-.} \tuplet 3/2 {r8-. dis-. b-.} b'4 |
                                      \acciaccatura c8 b4 g8--\( a--\) b4 e, |
                                      \acciaccatura g8 fis4 d8--\( e--\) fis4 b \( |
                                      c2. \) b4\( |
                                      a2. \) gis4~ |
                                      gis2. g4~ |
                                      g2. fis4~ |
                                      fis4 e2 dis4~ |
                                      dis2 e2 |
                                      e2. \fermata \bar "|."
                                    }	
                                  \\
                                    \new Voice \voiceOne {
                                      g4_\markup{ \dynamic{p} \italic "sotto voce" }|
                                      \acciaccatura a8 g4 e8--\( fis--\) g4 g, |
                                      b4. b8 b4 fis'4\( |
                                      b,4\) \tuplet 3/2 {r8-. g'-. fis-.} \tuplet 3/2 {r8-. dis-. b-.} g'4 \p |
                                      \acciaccatura a8 g4 e8--\( fis--\) g4 g, |
                                      \acciaccatura e'8 d4 b8--\( cis--\) d4 d \( \> |
                                      fis2. \) \! fis4~ |
                                      fis2. e4~ |
                                      e2. e4~_\markup{\italic "dim."} |
                                      e2. c4~ |
                                      c4 cis2 b4~ \< |
                                      b2~ \> b2 |
                                      b2. \pp  \bar "|."
                                    }
                                  >>
                                }
  bassonMusic = \relative c' { \clef bass
                              \key d \minor
                              <<
                                \new Voice \voiceOne {
                                  \partial 4 a4 \p |
                                  \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                  e4. a,8 a4 e'4--\( |
                                  a,4--\) r4 r a'4 |
                                  \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                  \acciaccatura ees8 d4 bes8--\( c--\) d4 
                                }	
                                \\
                                \new Voice \voiceOne {
                                  \partial 4 f4 |
                                  \acciaccatura g8 f4 d8 e f4 f, |
                                  a4. a8 a4 e'4 |
                                  a,4 r4 r f'4 |
                                  \acciaccatura g8 f4 d8 e f4 bes,_\markup{\italic "dimin."}	 |
                                  \acciaccatura c8 bes4 g8 a bes4 
                                }
                              >>  
                              g'\(\>^"à 2" |
                              cis,2.\) cis4\( |
                              d2.\) d4\( |
                              cis2.\) \pp r4 |
                              r1 |
                              r1 |
                              r1 |
                              r1 |
                              r2 r4 \fermata
                              <<
                                \new Voice \voiceOne {
                                  a'4 \p |
                                  \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                  e4. a,8 a4 e'4--\( |
                                  a,4--\) r4 r a'4 |
                                  \acciaccatura bes8 a4 f8--\( g--\) a4 d, |
                                  \acciaccatura ees8 d4 bes8--\( c--\) d4 
                                }	
                                \\
                                \new Voice \voiceOne {
                                  f4 |
                                  \acciaccatura g8 f4 d8 e f4 f, |
                                  a4. a8 a4 e'4 |
                                  a,4 r4 r f'4 |
                                  \acciaccatura g8 f4 d8 e f4 bes,_\markup{\italic "dimin."}	 |
                                  \acciaccatura c8 bes4 g8 a bes4 
                                }
                              >>  
                              g'\(\>^"à 2" |
                              cis,2.\) cis4\( |
                              d2.\) bes4\( |
                              f2.\) \pp r4 |
                              r1 |
                              r1 |
                              r1 |
                              r1 |
                              r2 r4 \fermata 
                              \bar "||"
                              \key d \major
                              r4 |
                              R1 |
                              r4 
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {a'8-. a-. a-.} a4
                                }
                                \\
                                \new Voice\voiceTwo {
                                  \tuplet 3/2 {d,8-._\markup{\dynamic{p} \italic "cresc."} d-. d-.} d4
                                }
                              >>
                              r4 |
                              R1 |
                              r4 
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {e8-. e-. e-.} e4
                                }
                                \\
                                \new Voice\voiceTwo {
                                  \tuplet 3/2 {a,8-. a-. a-.} a4
                                }
                              >>
                              r4 |
                              r2.
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {dis8-. e-. fis-.} | 
                                  g4 \acciaccatura a8 g8-. fis-. g4-> 
                                }
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {b,8-. cis-. dis-.} |
                                  e4 \acciaccatura fis8 e8-. dis-. e4-> 
                                }
                              >>
                              r4 |
                              r2.
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {eis8-. fis-. gis-.} | 
                                  a4 \acciaccatura b8 a8-. gis-. a4-> 
                                }
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {cis,8-. dis-. eis-.} |
                                  fis4 \acciaccatura gis8 fis8-. eis-. fis4-> 
                                }
                              >>
                              r4 |
                              r4 
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                  dis4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                  b2. \tuplet 3/2 {a'8-. a-. a-.} |
                                  a2. \tuplet 3/2 {fis8-. fis-. fis-.} |
                                  fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {d8-. d-. d-.} |
                                  d4 \tuplet 3/2 {a'8-. a-. a-.} a4 \tuplet 3/2 {d,8-. d-. d-.} |
                                  d4 \tuplet 3/2 {a'8-. a-. a-.} a4 \tuplet 3/2 {d,8-. f-. a-.} |
                                  bes4 \tuplet 3/2 {f8-. f-. f-.} f4 \tuplet 3/2 {d8-. f-. a-.} |
                                  bes4 \tuplet 3/2 {f8-. f-. f-.} f4 \tuplet 3/2 {d8-. f-. bes-.} |
 
                                }
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {e,,8-._\cresc e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                  dis4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                  b2. \ff \tuplet 3/2 {a'8-. a-. a-.} |
                                  a2. \tuplet 3/2 {fis8-. \< fis-. fis-.} |
                                  fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {d8-. d-. d-. }  |
                                  d4 \! \tuplet 3/2 {fis'8-. fis-. fis-.} fis4 \tuplet 3/2 {d,8-. d-. d-.} |
                                  d4 \tuplet 3/2 {fis'8-. fis-. fis-.} fis4 \tuplet 3/2 {d,8-. f-. a-.} |
                                  bes4 \tuplet 3/2 {d8-. d-. d-.} d4 \tuplet 3/2 {d,8-. f-. a-.} |
                                  bes4 \tuplet 3/2 {d8-. d-. d-.} d4 \tuplet 3/2 {d,8-. \ff f-. bes-.} |
                                }
                              >>
                              \key d \minor
                              <<
                                \new Voice \voiceOne {
                                  c'4 d8 \( c8 \) d4 \tuplet 3/2 {g,8 a bes~} |
                                  bes4 c d4. g,8 |
                                  g4 \tuplet 3/2 {ees,8-! bes'-! ees-!} f4-> g8 \( f \) |
                                  g4 \tuplet 3/2 {c,8 d ees~} ees4 f4 |
                                  g4. c,8 c4
                                } 
                                \\
                                \new Voice \voiceTwo {
                                  c4_"pesante" d8 \( c8 \) d4 \tuplet 3/2 {g,8 a bes~} |
                                  bes4 c d4. g8 |
                                  g4 ees,4 f4-> g8 \( f \) |
                                  g4 \tuplet 3/2 {c,8 d ees~} ees4 f4 |
                                  g4. c8 c4
                                }
                              >>
                              r4 |
                              R1 |
                              R1 |
                              r4 \tuplet 3/2 {ees,8^"à 2" \ff ees ees} ees2~ |
                              ees4 \tuplet 3/2 {ees8 ees ees} ees2~ |
                              ees4_\markup{ \italic "dim."} \tuplet 3/2 {ees8 ees ees} ees2~ |
                              ees4 \tuplet 3/2 {ees8 ees ees} ees4 \p r4 |
                              R1 |
                              R1 |
                              R1 |
                              R1 |
                              r2.
                              <<
                                \new Voice \voiceOne {
                                   a'4 \p |
                                   \acciaccatura bes8 a4 f8--\( g--\) a4 f |
                                   e4.-- a,8-- a4 
                                }	
                                \\
                                \new Voice \voiceOne {
                                   f'4 |
                                   \acciaccatura g8 f4 d8 e f4 d |
                                   a4. a8 a4 
                                }
                              >>
                              r4 |
                              r4 
                              <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {r8-. f'-. e-.} \tuplet 3/2 {r8-. cis-. a-.} a'4  |
                                  \acciaccatura bes8 a4 f8--\( g--\) a4 d,4 |
                                  \acciaccatura f8 e4 c8--\( d--\) e4 c' \( |
                                  cis2. \)  cis4~ |
                                  cis2. d4~ |
                                  d2. d4~ |
                                  d2. g,4~ |
                                  g4 f2 e4~ |
                                  e2 f2 |
                                  f2. \fermata \bar "|."
                                }
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {r8-. f-. e-.} \tuplet 3/2 {r8-. cis-. a-.} f'4 \p |
                                  \acciaccatura g8 f4 d8--\( e--\) f4 bes,4 |
                                  \acciaccatura d8 c4 a8--\( b--\) c4 a' \( \> |
                                  b2. \) \! a4 \( |
                                  g2. \) fis4~ |
                                  fis2. f4~_\markup{ \italic "dim." } |
                                  f2. e4~ |
                                  e4 d2 a4~ \< |
                                  a2 \> a2 |
                                  a2. \pp \fermata \bar "|."
                                }
                              >>
                            }                  
  % Key signature is often omitted for horns
  hornuntroisMusic = \transpose c' f
                     \relative c'' {
                       << 
                         \new Voice \voiceOne {
                           \partial 4 ees4 \p |
                           ees4 c8\( d\) ees2 |
                           d2. 
                         }
                       \\
                         \new Voice \voiceTwo {
                           \partial 4 c4 |
                           c4 c8 c c2 |
                           g2.
                         }
                       >>
                       r4 |
                       r2.
                       << 
                         \new Voice \voiceOne {
                           ees'4 |
                           ees4 c8\( d\) ees4
                         }
                       \\
                         \new Voice \voiceTwo {
                           c4 |
                           c4 c8 c c4 
                         }
                       >>
                       r4 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       r2 r4 \fermata
                       << 
                         \new Voice \voiceOne {
                           ees4 \p |
                           ees4 c8\( d\) ees2 |
                           d2. 
                         }
                       \\
                         \new Voice \voiceTwo {
                           c4 |
                           c4 c8 c c2 |
                           g2. 
                         }
                       >>
                       r4 |
                       r2.
                       << 
                         \new Voice \voiceOne {
                           ees'4 |
                           ees4 c8\( d\) ees4 
                         }
                       \\
                         \new Voice \voiceTwo {
                           c4 |
                           c4 c8 c c4 
                         }
                       >>
                       r4 |
                       R1 |
                       R1 |
                       R1 |
                       r2. r4 |
                       R1 |
                       R1 |
                       r4 aes^"à 2" \p ges aes8\( ges \) |
                       aes4 bes f aes |
                       <<
                         \new Voice \voiceOne {
                           aes4. \< g8 \! g4 \fermata
                         }
                         \\
                         \new Voice \voiceTwo {
                           aes4. c,8 c4
                         }
                       >>
                       r4 |
                       r2 r4
                       <<
                         \new Voice \voiceOne {
                           \tuplet 3/2 {e'8-. e-. e-.} |
                           e2.
                         }
                         \\
                         \new Voice \voiceTwo {
                           \tuplet 3/2 {c8-. \p c-. c-.} |
                           c2.
                         }
                       >>
                       r4 |
                       r2 r4
                       <<
                         \new Voice \voiceOne {
                           \tuplet 3/2 {e8-. f-. g-.} |
                           d2. d4-> |
                           d4 e8-. d-. e4-> 
                         }
                         \\
                         \new Voice \voiceTwo {
                           \tuplet 3/2 {c8-. d-. e-.} |
                           g,2. g4~ |
                           g4 c8-. b-. c4->
                         }
                       >>
                       r4 |
                       r2.
                       <<
                         \new Voice \voiceOne {
                           e4->^\markup{\dynamic{f}} |
                           dis4 dis8-. cis-. dis4
                         }
                         \\
                         \new Voice \voiceTwo {
                           r4 |
                           r4 b8-. \f  fis-. b4
                         }
                       >>
                       r4 |
                       r2. \tuplet 3/2 {e,8-._\markup{ \dynamic{f} \italic "cresc."}^"à 2" e-. e-.} |
                       e2. \tuplet 3/2 {e8-. e-. e-.} |
                       e4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {e8-. e-. e-.} |
                       e4 \ff b'8 a b2~ |
                       b4
                       <<
                         \new Voice \voiceOne {
                           g'8 f g4 \tuplet 3/2 {c,8-. c-. c-.} |
                           c4 \tuplet 3/2 {c8-. c-. c-.} c4 \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  ees4 |
                           des ees8 \( des \) bes4 aes |
                           ees' des bes4. aes8 |
                           aes4 f' ees des8 \( ees \) |
                           c4 bes f' ees |
                           c4. bes8 bes4 
                         }
                         \\
                         \new Voice \voiceTwo {
                           g8 f g4 \tuplet 3/2 {c,8-. \< c-. c-.} |
                           c4 \tuplet 3/2 {c8-. c-. c-.} c4 \tuplet 3/2 {c8-. c-. c-.} |
                           c2. \! \tuplet 3/2 {c8-. c-. c-.} |
                           c2. \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  \tuplet 3/2 {c8-. c-. c-.} |
                           c2.  c'4 \ff | 
                           bes4_"pesante" aes8 \( bes \) g4 f |
                           c' bes g4. f8 |
                           f4 des' c aes8 \( c \) |
                           a4 f des' c |
                           a4. f8 f4 
                         }
                       >>
                       \tuplet 3/2 {aes8^"à 2" \f aes aes} |
                       aes2. \tuplet 3/2 {aes8 aes aes} |
                       aes2. \tuplet 3/2 {aes8 \f aes aes} |
                       aes4 f' ees des8 \( ees \) |
                       c4 bes des c |
                       des_\markup{\italic "dim."} ees des c |
                       des ees des2 \p |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       r2. \tuplet 3/2 {r8^"à 2" r c,~ \pp} |
                       c1 |
                       g2. r4 |
                       r2. \tuplet 3/2 {r8 r c~ \pp} |
                       c2. \tuplet 3/2 {r8 r g~} |
                       g2. r4 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       R1 |
                       r2. \fermata \bar "|."
                     }
  horndeuxquatreMusic = \transpose c' f
                        \relative c { 
                          \partial 4 r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2 r4 \fermata r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. r4 |
                          R1 |
                          R1 |
                          r4 
                          <<
                            \new Voice \voiceOne {
                              bes''\p c d8\( c\) |
                              d4 g bes, c |
                              d4. \< e8 \! e4 \fermata
                            }
                            \\
                            \new Voice \voiceTwo {
                              bes a bes8 a |
                              bes4 bes g a |
                              a4. a8 a4 
                            }
                          >>
                          r4 |
                          r2. e4~^"1." \p |
                          e2. r4 |
                          r2. e4~^"1." \p |
                          e2. r4 |
                          R1 |
                          R1 |
                          R1 |
                          r2. \tuplet 3/2 {cis'8-._\markup{ \dynamic{f} \italic "cresc."}^"à 2" cis-. cis-.} |
                          cis2. \tuplet 3/2 {cis8-. cis-. cis-.} |
                          cis4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {cis8-. cis-. cis-.} |
                          cis4 \ff gis8 fis gis2~ |
                          gis4 e'8 \( d \) e4  \tuplet 3/2 {a,8-. \< a-. a-.} |
                          a4 \tuplet 3/2 {a8-. a-. a-.} a4 \tuplet 3/2 {a8-. a-. a-.} |
                          a2. \! \tuplet 3/2 {a8-. a-. a-.} |
                          a2.
                          <<
                            \new Voice \voiceOne {
                              \tuplet 3/2 {c8-. c-. c-.} |
                              c2. \tuplet 3/2 {c8-. c-. c-.} |
                              c2. a'4 |
                              g4 f8 \( g \) e4 d |
                              a' g e4. d8 |
                              d4
                            }
                            \\
                            \new Voice \voiceTwo {
                              \tuplet 3/2 {c,8-. c-. c-.} |
                              c2. \tuplet 3/2 {c8-. c-. c-.} |
                              c2. f'4 \ff |
                              e4_"pesante" c8 \( e \) cis4 a |
                              f' e cis4. a8 |
                              a4
                            }
                          >>
                          r4 r2 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r4 bes4^"à 2" \ff a bes8 \( a \) |
                          fis4 g d' c |
                          f_\markup{\italic "dim."} ees d c |
                          f ees d2 \p |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. \fermata \bar "|."
                        }
  trumpetMusic = \relative c { 
                          \partial 4 r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 | 
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2 r4 \fermata r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. r4 | 
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2 r4 \fermata 
                          r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. \tuplet 3/2 {e''8-._\markup{ \dynamic{f} \italic "cresc."}^"à 2" e-. e-.} |
                          e2.-> \tuplet 3/2 {e8-. e-. e-.} |
                          e4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {e8-. e-. e-.} |
                          e2. \ff 
                          <<
                            \new Voice \voiceOne {
                              \tuplet 3/2 {d8-. d-. d-.} |
                              d2.
                            }
                            \\
                            \new Voice \voiceTwo {
                              \tuplet 3/2 {g,8-. g-. g-.} |
                              g2.
                            }
                          >>
                          \tuplet 3/2 {c8-.^"à 2" \< c-. c-.} |
                          c4 \tuplet 3/2 {c8-. c-. c-.} c4 \tuplet 3/2 {c8-. c-. c-.} |
                          c2. \! \tuplet 3/2 {c8-. c-. c-.} |
                          c2. \tuplet 3/2 {c8-. c-. c-.} |
                          c2. \tuplet 3/2 {c8-. c-. c-.} |
                          c2. r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. \fermata \bar "|."
                        }
  tromboneundeuxMusic = \relative c { 
                          \clef tenor
                          \key d \minor
                          \partial 4 r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 | 
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2 r4 \fermata r4 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. r4 |
                          R1 |
                          R1 |
                          r4
                          <<
                            \new Voice \voiceOne {
                              ees'4 \pp f g8\( f \) |
                              g4 ees c d |
                              bes4. \< a8 \! a4 \fermata 
                            }
                            \\
                            \new Voice \voiceTwo {
                              ees'4 d ees8 d |
                              ees4 c ees, f |
                              g4. d8 d4 
                            }
                          >>
                          \bar "||"
                          \key d \major
                          r4 |
                          r2. 
                          <<
                            \new Voice \voiceOne {
                              d'4^\markup{\dynamic{p}} |
                              e4 fis8_\cresc \( e \) fis4 r4 |
                              R1 |
                              r2. d4 |
                              e4 fis8 \( e \) fis4 
                            }
                            \\
                            \new Voice \voiceTwo {
                              r4 |
                              R1 |
                              r2. a,4 |
                              b4 cis8 \( b \) cis4 r4 |
                              r2. 
                            }
                          >>
                          r4 |
                          r2. e4^"1." \mf |
                          eis4 eis8 \( dis \) eis4 r4 |
                          R1 |
                          r4
                          <<
                            \new Voice \voiceOne {
                              fis8 \( e \) fis2 |
                              fis4 \tuplet 3/2 {fis8 fis fis} fis4 \tuplet 3/2 {fis8 fis fis} |
                              fis4
                            }
                            \\
                            \new Voice \voiceTwo {
                              fis,8_\markup{ \dynamic{f} \italic "cresc."} \( e \) fis2 |
                              fis4 \tuplet 3/2 {fis8 fis fis} fis4 \tuplet 3/2 {fis8 fis fis} |
                              fis4 \ff
                            }
                          >>
                          cis'8^"à 2" \( b \) cis2~ |
                          cis4 a8 \( g \) a4 \tuplet 3/2 {d8 \< d d} |
                          d4 \tuplet 3/2 {d8 d d} d4 \tuplet 3/2 {d8 d d} |
                          d4 \! e8 \( d \) fis2~ |
                          fis4 e8 \( d \) fis4 r |
                          r c8 \( bes \) d2~ |
                          d4 c8 \( bes \) d4 r4 |
                          \bar "||"
                          \key d \minor
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r4
                          <<
                            \new Voice \voiceOne {
                              e,2. \( |
                              f4^\markup{\italic "riten."} \) e2.~ |
                              e2 s2 |
                            }
                            \\
                            \new Voice \voiceTwo {
                              c2.~ \pp |
                              c4 cis2.~ |
                              cis2 \> r2 \! |
                            }
                          >>
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          R1 |
                          r2. \fermata \bar "|."
                        }
                        
  trombonetroisTubaMusic = \relative c { 
                             \clef bass 
                             \key d \minor
                             \partial 4 r4 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 | 
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             r2 r4 \fermata r4 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             R1 |
                             r2. r4 |
                             R1 |
                             R1 |
                             <<
                               \new Voice \voiceOne {
                                 r4 g'4^\markup{\dynamic pp} f ees8\( f \) |
                                 ees4 aes, c bes |
                                 d4. \< a8 \! a4 \fermata 
                               }
                               \\
                               \new Voice \voiceTwo {
                                 r1 |
                                 r2 c4_\markup{\dynamic pp} bes |
                                 g4. d8 d4 
                               }
                             >>
                             \bar "||"
                             \key d \major
                             r4 |
                              r2. 
                          <<
                            \new Voice \voiceOne {
                              d'4^\markup{\dynamic{p}} |
                              e4 fis8_\cresc \( e \) fis4 r4 |
                              R1 |
                              r2. d4 |
                              e4 fis8 \( e \) fis4 
                            }
                            \\
                            \new Voice \voiceTwo {
                              r4 |
                              R1 |
                              r2. a,4 |
                              b4 cis8 \( b \) cis4 r4 |
                              r2. 
                            }
                          >>
                          r4 |
                           r2. e4^"3." \mf |
                          eis4 eis8 \( dis \) eis4 r4 |
                          R1 |
                          r4
                           <<
                                \new Voice \voiceOne {
                                  \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                  dis4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                  b4
                                }
                                
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {e,8-._\cresc e-. e-.} e4 \tuplet 3/2 {dis8-. dis-. dis-.} |
                                  dis4 \tuplet 3/2 {cis8-. cis-. cis-.} cis4 \tuplet 3/2 {b8-. b-. b-.} |
                                  b4 \ff 
                                }
                              >>
                           cis'8 \( b \) cis2~ |
                           cis4
                           <<
                             \new Voice \voiceOne {
                                  \tuplet 3/2 {g'8-. g-. g-.} g4 \tuplet 3/2 {fis8-. fis-. fis-.} |
                                  fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {d8-. d-. d-.} |
                                  d4 
 
                                }
                                \\
                                \new Voice \voiceTwo {
                                  \tuplet 3/2 {g,8-. g-. g-.} g4 \tuplet 3/2 {fis8-. \< fis-. fis-.} |
                                  fis4 \tuplet 3/2 {e8-. e-. e-.} e4 \tuplet 3/2 {d8-. d-. d-. }  |
                                  d4 \! 
                                }
                              >>
                            e'8^"à 2" \( d \) fis2~ |
                            fis4 e8 \( d \) fis4 r |
                            r c8 \( bes \) d2~ |
                            d4 c8 \( bes \) d4 r |
                            \key d \minor
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            r2. 
                            <<
                              \new Voice \voiceOne {
                                \tuplet 3/2 {bes8-. bes-. bes-. } |
                                bes2. \tuplet 3/2 {bes8-. bes-. bes-. } |
                                bes2. \tuplet 3/2 {bes8-. bes-. bes-. } |
                                bes4
                              }
                              \\
                              \new Voice \voiceTwo {
                                \tuplet 3/2 {bes,8-. \f bes-. bes-. } |
                                bes2. \tuplet 3/2 {bes8-. bes-. bes-. } |
                                bes2. \tuplet 3/2 {bes8-. bes-. bes-. } |
                                bes4
                              }
                            >>
                            r4 r2 |
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            r4
                            <<
                              \new Voice \voiceOne {
                                bes'2. \( |
                                a4 \)
                              }
                              \\
                              \new Voice \voiceTwo {
                                bes2. \( \pp |
                                f4 \)
                              }
                            >>
                            bes2^"Tuba" \( a8 g |
                            f4 e4 \) \> r \! \tuplet 3/2 {r8 r d8~ \pp } |
                            d1 |
                            a2. r4 |
                            r2. \tuplet 3/2 {r8^"à 2" r d8~ \pp } |
                            d2. \tuplet 3/2 {r8 r a8~  } |
                            a2. r4 |
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            R1 |
                            r2. \fermata \bar "|."
                        }
  timbalesMusic = \relative c { 
                    \clef bass
                    \partial 4 r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2 r4 \fermata r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2. r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2 r4 \fermata 
                    \bar "||"
                    r4 |
                    r4 \tuplet 3/2 {d8 \p d d  } d4 r |
                    r4 \tuplet 3/2 {d8 \pp d d  } d4 r |
                    r4 \tuplet 3/2 {a8 \p a a  } a4 r |
                    r4 \tuplet 3/2 {a8 \pp a a  } a4 r |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2. \tuplet 3/2 {a8 \ff a a  } |
                    a2. \tuplet 3/2 {d8 \< d d  } |
                    d4 \tuplet 3/2 {d8 d d  } d4 \tuplet 3/2 {d8 d d  } |
                    d2 \! \startTrillSpan d4 \stopTrillSpan \tuplet 3/2 {d8 d d  } |
                    d2 \startTrillSpan d4 \stopTrillSpan \tuplet 3/2 {d8 d d  } |
                    d2_\markup{ \italic "dim."} \startTrillSpan d4 \stopTrillSpan \tuplet 3/2 {d8 d d  } |
                    d2 \startTrillSpan d4 \stopTrillSpan r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2. \fermata \bar "|."
  }
  
  violinIMusic = \relative c' { 
                   \key d \minor
                   \partial 4 r4^\markup{ \bold "Andante"}|
                   R1 |
                   R1 |
                   r4 e4\( \pp a,4\) r4 |
                   R1 |
                   R1 |
                   R1 |
                   R1 |
                   r2 r4 d'4 \p |
                   c4 bes8\( c\) a4 g |
                   d' c a4. g8 |
                   g4 g f ees8\( f\) |
                   d4 c g'\< f |
                   bes4. \! \> a8 a4 \! \fermata r4 |
                   R1 |
                   R1 |
                   r4 e4\( \pp a,4\) r4 |
                   R1 |
                   R1 |
                   R1 |
                   R1 |
                   r2 r4 bes'4 \p |
                   c d8\( c\) d4 g |
                   bes, c d4. g,8 |
                   g4 r4 r2 |
                   R1 |
                   r2 r4 \fermata \bar "||"
                   \key d \major
                   \tuplet 3/2 { <fis fis'>8-.^\markup{ \bold "Allegro ma non troppo"} \p  <fis fis'>-. <fis fis'>-.}  |
                   <fis fis'>2. r4 |
                   r2. \tuplet 3/2 { <fis fis'>8-._\cresc  <g g'>-. <a a'>-.}  |
                   <e e'>2. r4 |
                   r2. \tuplet 3/2 {fis'8-. fis-. fis-. } |
                   fis2.-> \tuplet 3/2 {fis8-. e-. fis-. } |
                   g2.-> \tuplet 3/2 {gis8-. \f gis-. gis-. } |
                   gis2.-> \tuplet 3/2 {gis8-. fis-. gis-. } |
                   a2.-> \tuplet 3/2 {ais8-._\cresc ais-. ais-. } |
                   ais2.-> \tuplet 3/2 {b8-. b-. b-. } |
                   b4 \tuplet 3/2 {cis8-. cis-. cis-. } cis4 \tuplet 3/2 {dis8-. dis-. dis-. } |
                   dis2. \ff \tuplet 3/2 {e8-. e-. e-. } |
                   e2. \tuplet 3/2 {fis8 \< fis fis } |
                   fis4 \tuplet 3/2 {g8 g g} g4 \tuplet 3/2 {a8 a a} |
                   a2~ \! a8 r \tuplet 3/2 {a8 a a} |
                   a2~ a8 r \tuplet 3/2 {a8 a a} |
                   bes2~ bes8 r \tuplet 3/2 {a8 a a} |
                   bes2~ bes8 r d,4 |
                   \key d \minor 
                   c_"pesante" bes8 \( c \) a4  g |
                   d'4 c a4. g8 |
                   g4 g-> f-> ees8 \( f \) |
                   d4 c g' f |
                   d4. c8 c4 d_\markup{ \italic "sempre" \dynamic{ff}} |
                   c bes8 \( c \) aes4 g |
                   d' c aes4. g8 |
                   g4 g f ees8 \( f \) |
                   d4 c g' f |
                   bes^"Poco a poco riten."_\markup{\italic "dim."} aes g f |
                   bes aes g2 |
                   f ees d des |
                   c4 \p r4 r2 |
                   R1 |
                   r2. \tuplet 3/2 {r8^"Pizz." \p a'' d,} |
                   \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r f d} |
                   \tuplet 3/2 {r f' e} \tuplet 3/2 {r cis a} \tuplet 3/2 {r f'' e} \tuplet 3/2 {r f, e} |
                   \tuplet 3/2 {r cis a} e4^"Arco" \pp \( a, \) \tuplet 3/2 {r8^"Pizz." \p a'' d,} |
                   \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r d a} |
                   \tuplet 3/2 {r f e} \tuplet 3/2 {r c d} \tuplet 3/2 {r f e} \tuplet 3/2 {r c a} |
                   \tuplet 3/2 {r^"Arco" \pp a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} r4 |
                   R1 |
                   \tuplet 3/2 {r8 \p bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} r4 |
                   R1 |
                   R1 |
                   R1 |
                   r2. \fermata \bar "|."
                 }
                 
  violinIIMusic = \relative c' { 
                    \key d \minor 
                    \partial 4 r4 |
                    R1 |
                    R1 |
                    r4 e4\( \pp a,4\) r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2 r4 bes'4 \p |
                    a4 f8\( a\) fis4 g |
                    bes a fis4. g8 |
                    g4 ees d bes8\( d\) |
                    b4 c ees\< d |
                    d4. \! \> cis8 e4 \! \fermata r4|
                    R1 |
                    R1 |
                    r4 e4\( \pp a,4\) r4 |
                    R1 |
                    R1 |
                    R1 |
                    R1 |
                    r2 r4 d4 \p |
                    ees f8\( ees\) f4 g |
                    g g fis4. g8 |
                    g4 r4 r2 |
                    R1 |
                    r2 r4 \fermata \bar "||"
                    \key d \major
                    \tuplet 3/2 { <d d'>8-. \p  <d d'>-. <d d'>-.}  |
                   <d d'>2. r4 |
                   r2. \tuplet 3/2 { <d d'>8-._\cresc  <e e'>-. <fis fis'>-.}  |
                   <a, a'>2. r4 |
                   r2. \tuplet 3/2 {d'8-. d-. d-. } |
                   d2.-> \tuplet 3/2 {dis8-. cis-. b-. } |
                   b2.-> \tuplet 3/2 {b8-. \f b-. b-. } |
                   b2.-> \tuplet 3/2 {eis8-. dis-. cis-. } |
                   cis2.-> \tuplet 3/2 {ais8-._\cresc ais-. ais-. } |
                   ais2.-> \tuplet 3/2 {b8-. b-. b-. } |
                   b4 \tuplet 3/2 {cis8-. cis-. cis-. } cis4 \tuplet 3/2 {dis8-. dis-. dis-. } |
                   dis2. \ff \tuplet 3/2 {e8-. e-. e-. } |
                   e2. \tuplet 3/2 {fis8 \< fis fis } |
                   fis4 \tuplet 3/2 {g8 g g} g4 \tuplet 3/2 {a8 a a} |
                   a2~ \! a8 r \tuplet 3/2 {a8 a a} |
                   a2~ a8 r \tuplet 3/2 {a8 a a} |
                   bes2~ bes8 r \tuplet 3/2 {a8 a a} |
                   bes2~ bes8 r d,4 |
                   \key d \minor 
                   c_"pesante" bes8 \( c \) a4  g |
                   d'4 c a4. g8 |
                   g4 g-> f-> ees8 \( f \) |
                   d4 c g' f |
                   d4. c8 c4 aes'_\markup{ \italic "sempre" \dynamic{ff}} |
                   aes aes8 \( aes \) f4 ees |
                   aes aes f4. ees8 |
                   ees4 ees d ees8 \( d \) |
                   b4 c ees d |
                   ees_\markup{\italic "dim."} f ees d |
                   ees f ees2 |
                   d bes |
                   bes bes |
                   a4 \p r4 r2 |
                   R1 |
                   r2. \tuplet 3/2 {r8^"Pizz." \p a'' d,} |
                   \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r f d} |
                   \tuplet 3/2 {r f' e} \tuplet 3/2 {r cis a} \tuplet 3/2 {r f' e} \tuplet 3/2 {r f e} |
                   \tuplet 3/2 {r cis a} e4^"Arco" \pp \( a, \) \tuplet 3/2 {r8^"Pizz." \p a'' d,} |
                   \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r d a} |
                   \tuplet 3/2 {r f e} \tuplet 3/2 {r c d} \tuplet 3/2 {r f e} \tuplet 3/2 {r c a} |
                   R1 |
                   \tuplet 3/2 {r8^"Arco" \pp a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} r4 |
                   \tuplet 3/2 {r8 \p bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} r4 |
                   \tuplet 3/2 {r8_\markup{\italic "dim."} a-. \( gis-. \)} \tuplet 3/2 {r a-. \( gis-. \)} \tuplet 3/2 {r a-. \( gis-. \)} r4 |
                   R1 |
                   R1 |
                   r2. \fermata \bar "|."
                  }
  violaMusic = \relative c' { 
                 \clef alto 
                 \key d \minor  
                 \partial 4 r4 |
                 R1 |
                 R1 |
                 r4 e4\( \pp a,4\) r4 |
                 R1 |
                 r2 r4 g4\( \pp |
                 g1\) |
                 R1 |
                 r2 r4 f'4 \p |
                 ees4 d8\( ees\) c4 bes |
                 f' ees c4. bes8 |
                 bes4 bes aes g8\( aes\) |
                 f4 ees bes'\< aes |
                 bes4. \! \> a?8 a4 \! \fermata r4|
                 R1 |
                 R1 |
                 r4 e4\( \pp a,4\) r4 |
                 R1 |
                 r2 r4 g'4\( \pp  \>|
                 g1\) |
                 r2. d4 |
                 c2. \pp <d' bes'>4 \p |
                 <c a'> <bes bes'>8\( <c a'> \) <bes bes'>4 bes' |
                 d, c a4. bes8 |
                 bes4 r4 r2 |
                 R1 |
                 r2 r4 \fermata \bar "||"
                 \key d \major
                 d,4 \mp |
                 e fis8 \( e \) fis4 r |
                 r2.a4_\cresc |
                 b4 cis8 \( b \) cis4 d |
                 R1 |
                 r2. \tuplet 3/2 {b8-. \f cis-. dis-. } |
                 e4 \acciaccatura fis8 e8-. dis-. e4-> r4 |
                 r2. \tuplet 3/2 {cis8-. dis-. eis-. } |
                 fis4 \acciaccatura gis8 fis-. eis-. fis4-> r4 |
                 r2. 
                 <<
                   \new Voice \voiceOne {
                     \tuplet 3/2 {fis8^"div." fis fis} |
                     fis4 \tuplet 3/2 {fis8 fis fis} fis4 \tuplet 3/2 {fis8 fis fis} |
                     fis2. \tuplet 3/2 {<cis a'>8 <cis a'> <cis a'>} |
                     <cis a'>2. \tuplet 3/2 {d8 d d} |
                     d4 \tuplet 3/2 {e8 e e} e4 \tuplet 3/2 {fis8 fis fis} |
                     fis2~  fis8 r \tuplet 3/2 {fis8 fis fis} |
                     fis2~  fis8 r \tuplet 3/2 {f8 f f} |
                     f2~  f8 r \tuplet 3/2 {f8 f f} |
                     f2~  f8 r 
                   }
                   \\
                   \new Voice \voiceTwo {
                     \tuplet 3/2 {fis,8_\cresc fis fis} |
                     fis4 \tuplet 3/2 {fis8 fis fis} fis4 \tuplet 3/2 {fis8 fis fis} |
                     fis2. \ff \tuplet 3/2 {a8 a a} |
                     a2. \tuplet 3/2 {d,8 \< d d} |
                     d4 \tuplet 3/2 {e8 e e} e4 \tuplet 3/2 {fis8 fis fis} |
                     fis2~ \! fis8 r \tuplet 3/2 {fis8 fis fis} |
                     fis2~ \! fis8 r \tuplet 3/2 {f8 f f} |
                     f2~ \! f8 r \tuplet 3/2 {f8 f f} |
                     f2~ \! f8 r 
                   }
                 >>
                 \tuplet 3/2 {d8 \ff f bes} |
                 \key d \minor 
                 c4_"pesante" d8 \( c \) d4 \tuplet 3/2 {g,8 a bes~} |
                 bes4 c d4. g,8 |
                 g4 \tuplet 3/2 {ees8-! bes'-! ees-!} f4-> g,8 \( f \) |
                 g4 \tuplet 3/2 {c,8 d ees~} ees4 f4 |
                 g4. c,8 c4 f'_\markup{ \italic "sempre" \dynamic{ff}} |
                 ees f8 \( ees \) bes4 bes |
                 f' ees bes4. bes8 |
                 bes4 bes aes bes8 \( aes \) |
                 f4 ees bes' aes |
                 bes_\markup{\italic "dim."} bes bes aes |
                 bes bes bes2 |
                 aes g |
                 g g |
                 f8 \p r r4 r2 |
                 R1 |
                 r2. \tuplet 3/2 {r8^"Pizz." \p a' d,} |
                 \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r f d} |
                 \tuplet 3/2 {r f' e} \tuplet 3/2 {r cis a} \tuplet 3/2 {r f' e} \tuplet 3/2 {r f e} |
                 \tuplet 3/2 {r cis a} e'4^"Arco" \pp \( a, \) \tuplet 3/2 {r8^"Pizz." \p a' d,} |
                 \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r d a} |
                 \tuplet 3/2 {r f e} \tuplet 3/2 {r c d} \tuplet 3/2 {r f e} \tuplet 3/2 {r c' a} |
                 \tuplet 3/2 {r8^"Arco" \pp a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} r4 |
                 \tuplet 3/2 {r8 a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} r4 |
                 \tuplet 3/2 {r8 \p bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} \tuplet 3/2 {r bes-. \( a-. \)} r4 |
                 \tuplet 3/2 {r8_\markup{\italic "dim."} a-. \( gis-. \)} \tuplet 3/2 {r a \( gis \)} \tuplet 3/2 {r a-. \( gis-. \)} \tuplet 3/2 {r fis-. \( g-.~ \)} |
                 g4 \tuplet 3/2 {r8 g \( gis~ \)} gis4 a4 \< |
                 f2 \> d2 |
                 d2. \pp \fermata \bar "|."
               }
               
  celloMusic = \relative c { 
                 \clef bass 
                 \key d \minor 
                 \partial 4 d4 \p|
                 d d8--\( d--\) d4 d |
                 a4. a8 a4 r4 |
                 r4 e'4\( \pp a,4\) d \p|
                 d d8--\( d--\) d4 g_\markup{\italic "dimin."} |
                 g g8--\( g--\) g4 ees\(\> |
                 ees\) bes2 bes4\( |
                 bes\) f2. |
                 a2. \pp bes4 \p |
                 c4 d8\( c\) d4 g |
                 bes, c d4. g,8 |
                 g4 ees f g8\( f\) |
                 g4 c ees,\< f |
                 g4. \! \> a8 a4 \! \fermata d4|
                 d d8--\( d--\) d4 d |
                 a4. a8 a4 r4 |
                 r4 e'4\( \pp a,4\) d \p|
                 d d8--\( d--\) d4 g_\markup{\italic "dimin."} |
                 g g8--\( g--\) g4 ees\(\> |
                 ees\) bes2 bes4\( |
                 bes\) f2. |
                 a2. \pp bes4\( \p |
                 bes2.\) ees4 |
                 g ees d4. g8 |
                 g4 r4 r2 |               
                 R1 |
                 r2 r4 \fermata \bar "||"
                 \key d \major
                 d4 \mp |
                 e4 fis8 \( e8 \) fis4 r4 |
                 r2. a,4_\cresc |
                 b cis8 \( b \) cis4 d |
                 R1 |
                   r4 \tuplet 3/2 {b8-. b-. b-. } b4 r4 |
                   r4 \tuplet 3/2 {e,8-. e-. e-. } e4 r4 |
                   r4 \tuplet 3/2 {cis'8-. \f cis-. cis-. } cis4 r4 |
                   r4 \tuplet 3/2 {fis,8-. fis-. fis-. } fis4 r4 |
                   r4 \tuplet 3/2 {e'8_\cresc e e} e4 \tuplet 3/2 {dis8-. dis-. dis-. } |
                   dis4 \tuplet 3/2 {cis8-. cis-. cis-. } cis4 \tuplet 3/2 {b8-. b-. b-. } |
                   b2. \ff \tuplet 3/2 {a8 a a } |
                   a2. \tuplet 3/2 {fis8 \< fis fis} | 
                   fis4 \tuplet 3/2 {e8 e e} e4 \tuplet 3/2 {d8 d d} |
                   d2~ \! d8 r  \tuplet 3/2 {d8 d d} |
                   d2~  d8 r \tuplet 3/2 {d8 f a} |
                   bes2~  bes8 r \tuplet 3/2 {d,8 f a} |
                   bes2~  bes8 r \tuplet 3/2 {d,8 f bes} |
                   \key d \minor
                   c4_"pesante" d8 \( c \) d4 \tuplet 3/2 {g,8 a bes~} |
                 bes4 c d4. d8 |
                 d4 \tuplet 3/2 {ees,8-! bes'-! ees-!} f4-> g8 \( f \) |
                 g4 \tuplet 3/2 {c,,8 d ees~} ees4 f4 |
                 g4. g8 g4 bes_\markup{ \italic "sempre" \dynamic{ff}} |
                 c d8 \( c \) d4 ees |
                 bes c d4. ees8 |
                 ees4 ees, f g8 \( f \) |
                 g4 c ees, f |
                 g_\markup{\italic "dim."} d ees f |
                 g d ees f~ |
                 f g2  ees4~ |
                 ees e2 f4~ |
                 f8 \p r r4 r2 |
                 R1 |
                 r2. \tuplet 3/2 {r8^"Pizz." \p a' d,} |
                 \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r f d} |
                 \tuplet 3/2 {r f' e} \tuplet 3/2 {r cis a} \tuplet 3/2 {r f'' e} \tuplet 3/2 {r f, e} |
                 \tuplet 3/2 {r cis a} e'4^"Arco" \pp \( a, \) \tuplet 3/2 {r8^"Pizz." \p a' d,} |
                 \tuplet 3/2 {r bes a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r d a} |
                 \tuplet 3/2 {r f' e} \tuplet 3/2 {r c d} \tuplet 3/2 {r f e} \tuplet 3/2 {r c a} |
                 \tuplet 3/2 {r8^"Arco" \pp a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} r4 |
                 \tuplet 3/2 {r8 a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} r4 |
                 \tuplet 3/2 {r8 \p d4~ } d2 \tuplet 3/2 {r8_\markup{\italic "dim."} a-. \( gis-. \)} |
                 <<
                   \new Voice \voiceOne {
                     r2.^"div." \tuplet 3/2 {r8 fis \( g~ \)}  |
                     g4 \tuplet 3/2 {r8 g \( gis~ \)} gis4
                   }
                   \\
                   \new Voice \voiceTwo {
                     \tuplet 3/2 { r8 d4~ } d2 d4~ |
                     d2.
                   }
                 >>
                 a'4^"unis" \< |
                 f2 \> d2 |
                 d2. \pp \fermata \bar "|."
               }
  bassMusic = \relative c, { 
                \clef "bass_8" 
                \key d \minor 
                \partial 4 d4 \p|
                d d8--\( d--\) d4 d |
                a4. a8 a4 r4 |
                r4 e'4\( \pp a,4\) d \p|
                d d8--\( d--\) d4 g_\markup{\italic "dimin."} |
                g g8--\( g--\) g4 ees\(\> |
                ees2.\) bes4\( |
                bes1\) |
                a2. \pp bes4 \p |
                c4 d8\( c\) d4 g |
                bes, c d4. g,8 |
                g4 ees f g8\( f\) |
                g4 c ees,\< f |
                g4. \! \> a8 a4 \! \fermata d4|
                d d8--\( d--\) d4 d |
                a4. a8 a4 r4 |
                r4 e'4\( \pp a,4\) d \p|
                d d8--\( d--\) d4 g_\markup{\italic "dimin."} |
                g g8--\( g--\) g4 ees\(\> |
                ees2.\) bes4\( |
                bes1\) |
                a2. \pp r4|
                R1 |
                R1 |
                R1 |
                R1 |
                r2 r4 \fermata 
                \bar "||"
                \pageBreak
                \key d \major
                 r4 |
                 r4 \tuplet 3/2 {<d a'>8-. \p <d a'>-. <d a'>-. } <d a'>4 r4 |
                 r4 \tuplet 3/2 {<d a'>8-._\cresc <d a'>-. <d a'>-. } <d a'>4 r4 |
                 r4 \tuplet 3/2 {<a e'>8-. <a e'>-. <a e'>-. } <a e'>4 r4 |
                 r4 \tuplet 3/2 {<a e'>8-. <a e'>-. <a e'>-. } <a e'>4 r4 |
                   r4 \tuplet 3/2 {b8-. b-. b-. } b4 r4 |
                   r4 \tuplet 3/2 {e,8-. e-. e-. } e4 r4 |
                   r4 \tuplet 3/2 {cis'8-. \f cis-. cis-. } cis4 r4 |
                   r4 \tuplet 3/2 {fis,8-. fis-. fis-. } fis4 r4 |
                   r4 \tuplet 3/2 {e'8_\cresc e e} e4 \tuplet 3/2 {dis8-. dis-. dis-. } |
                   dis4 \tuplet 3/2 {cis8-. cis-. cis-. } cis4 \tuplet 3/2 {b8-. b-. b-. } |
                   b2. \ff \tuplet 3/2 {a8 a a } |
                   a2. \tuplet 3/2 {fis8 \< fis fis} | 
                   fis4 \tuplet 3/2 {e8 e e} e4 \tuplet 3/2 {d8 d d} |
                   d2~ \! d8 r  \tuplet 3/2 {d8 d d} |
                   d2~  d8 r \tuplet 3/2 {d8 f a} |
                   bes2~  bes8 r \tuplet 3/2 {d,8 f a} |
                   bes2~  bes8 r \tuplet 3/2 {d,8 f bes} |
                   \key d \minor
                   c4_"pesante" d8 \( c \) d4 \tuplet 3/2 {g,8 a bes~} |
                 bes4 c d4. d8 |
                 d4 \tuplet 3/2 {ees,8-! bes'-! ees-!} f4-> g8 \( f \) |
                 g4 \tuplet 3/2 {c,,8 d ees~} ees4 f4 |
                 <g c>4. c8 c4 \tuplet 3/2 {bes8_"marcato"_\markup{ \italic "sempre" \dynamic{ff}} bes bes} |
                 bes2. \tuplet 3/2 {bes8 bes bes} |
                 bes2. \tuplet 3/2 {bes8 bes bes} |
                 bes4 \tuplet 3/2 {<ees, ees'>8 <ees ees'> <ees ees'>} <ees ees'>2~ |
                 <ees ees'>4 \tuplet 3/2 {<ees ees'>8 <ees ees'> <ees ees'>} <ees ees'>2~ |
                 <ees ees'>4 \tuplet 3/2 {<ees ees'>8_\markup{\italic "dim"} <ees ees'> <ees ees'>} <ees ees'>2~ |
                 <ees ees'>4 \tuplet 3/2 {<ees ees'>8 <ees ees'> <ees ees'>} <ees ees'>4 f~ |
                 f g2  ees4~ |
                 ees e2 f4~ |
                 f8 \p r r4 r2 |
                 R1 |
                 r2. \tuplet 3/2 {r8^"Pizz." \p a' d,} |
                 \tuplet 3/2 {r bes' a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r f d} |
                 \tuplet 3/2 {r f e} \tuplet 3/2 {r cis a} r2 |
                 r4 \tuplet 3/2 {r8 \pp f' e} \tuplet 3/2 {r cis a} \tuplet 3/2 {r8 \p a' d,} |
                 \tuplet 3/2 {r bes' a} \tuplet 3/2 {r f g} \tuplet 3/2 {r bes a} \tuplet 3/2 {r d, a} |
                 \tuplet 3/2 {r f' e} \tuplet 3/2 {r c d} \tuplet 3/2 {r f e} \tuplet 3/2 {r c a} |
                 \tuplet 3/2 {r8^"Arco" \pp a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} \tuplet 3/2 {r a-. \( g-. \)} r4 |
                 \tuplet 3/2 {r8 a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} \tuplet 3/2 {r a-. \( bes-. \)} r4 |
                 \tuplet 3/2 {r8 \p d,4~ } d2 \tuplet 3/2 {r8_\markup{\italic "dim."} a'-. \( gis-. \)} |
                 \tuplet 3/2 { r8 d4~ } d2 d4~ |
                 d2. a'4 \< |
                 f2 \> d2 |
                 d2. \pp \fermata \bar "|."
              }	

\header	{
  title = "Ballade pour piano"
  %subtitle = "n°1"
  composer = "J. Brahms, Op. 10, n°1"
  arranger = "arr : L. Feisthauer"
}

\score {
    <<
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new Staff = "Staff_flute" \with {
          instrumentName = #"Flûtes"
          shortInstrumentName =#"Flt."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \fluteMusic
        }
        \new Staff = "Staff_hautbois" \with {
          instrumentName = #"Hautbois"
          shortInstrumentName = #"Hb."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \hautboisMusic
        }
        \new Staff = "Staff_clarinet" \with {
          instrumentName = \markup { \right-column { "Clarinettes" \line { "en Si" \flat } }}
          shortInstrumentName = \markup { \center-column { "Clar." \line {"Si"\flat}}}
        }
        {
          % Declare that written Middle C in the music
          % to follow sounds a concert B flat, for
          % output using sounded pitches such as MIDI.
            \transposition bes
          % Print music for a B-flat clarinet
            \transpose bes c' \clarinetMusic
        }
        \new Staff = "Staff_basson" \with {
          instrumentName = #"Bassons"
          shortInstrumentName = #"Bsn."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \bassonMusic
        }
      >>
      \new StaffGroup = "StaffGroup_brass" <<
        \new GrandStaff = "GrandStaff_cors" <<
          \new Staff = "Staff_horn13" \with{
            instrumentName = \markup{ \right-column{"Cors I & II" \line {"en Ré"}}}
            shortInstrumentName = \markup{ \center-column{"Cors" \line {"Ré"}}}
          }
          {
            \transposition f
            \transpose f c' \hornuntroisMusic
          }
          \new Staff = "Staff_horn24" \with{
            instrumentName = \markup{ \right-column{"Cors III & IV" \line {"en Fa"}}}
            shortInstrumentName = \markup{ \center-column{"Cors" \line {"Fa"}}}
          }
          {
            \transposition f
            \transpose f c' \horndeuxquatreMusic
          }
        >>
        \new Staff = "Staff_trumpet" \with{
          instrumentName = \markup{ \right-column{"Trompettes" \line {"en Ré"}}}
          shortInstrumentName = \markup{ \center-column{"Tpt." \line {"Ré"}}}
        }
        {
          \trumpetMusic
        }
        \new GrandStaff = "GrandStaff_trombones" <<
          \new Staff = "Staff_trombones" \with{
            instrumentName = \markup{ \right-column{"Trombones" \line {"I & II"}}}
            shortInstrumentName = \markup{ \center-column{"Tbn." \line {"I & II"}}}
          }
          {
            \tromboneundeuxMusic
          }
          \new Staff = "Staff_trombonetuba" \with{
            instrumentName = \markup{ \right-column{"Trombone III" \line {"Tuba"}}}
            shortInstrumentName = \markup{ \center-column{"Tbn III" \line {"Tba"}}}
          }
          {
            \trombonetroisTubaMusic
          }
        >>
      >>  
      \new StaffGroup = "StaffGroup_percus" <<
        \new Staff = "Staff_timbales" \with{
            instrumentName = \markup{ \right-column{"Timbales" \line {"Ré / La"}}}
            shortInstrumentName = \markup{ \center-column{"Timb." \line {"Ré/La"}}}
        }
        {
          \timbalesMusic
        }
      >>
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" \with{
            instrumentName = #"Violons I"
            shortInstrumentName = #"Vlns I"
          }
          {
            \violinIMusic
          }
          \new Staff = "Staff_violinII" \with{
            instrumentName = #"Violons II"
            shortInstrumentName = #"Vlns II"
          }
          {
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" \with{
          instrumentName = #"Altos"
          shortInstrumentName = #"Altos"
        }
        {
          \violaMusic
        }
        \new Staff = "Staff_cello" \with{
          instrumentName = #"Violoncelles"
          shortInstrumentName = #"Vlcs"
        }
        {
          \celloMusic
        }
        \new Staff = "Staff_bass" \with{
          instrumentName = #"Contrebasses"
          shortInstrumentName = #"Cbs"
        }
        {
          \bassMusic
        }
      >>
>>

\layout {
   #(layout-set-staff-size 15)

  }
}