\version  "2.18.2"

%#(set-global-staff-size 17)
\paper {
  indent = 2.5\cm % space for instrumentName
  short-indent = 1\cm % space for shortInstrumentName
}
  fluteMusic = \relative a' 
                 { \key des \major
                   \time 3/8
                   \tempo "Mouvement de menuet"
                   << 
                     
                     \new Voice \voiceOne 
                     {
                       des8-- \p des-- des-- |
                       des8-- des-- des-- |
                       des-- c-- bes-- |
                       c-- ees4-> |
                     }
                     \\
                     \new Voice \voiceTwo
                     {
                       \relative a'
                       aes8 bes c |
                       bes c bes |
                       f' f f |
                       f, aes4 |
                     }
                   >>
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   f'8^"1." \p \( c'4 \) |
                   aes16 \( bes c4 \) |
                   f8-. \( f16-. f-. ees-. f-. \) |
                   des8 \( c4 \) |
                   c8 \< \( bes16 c aes g \) \! |
                   bes4 \(  f16 \> aes  |
                   c8 \) r8 \! r8 | 
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   
                   <f, aes>4. \p |
                   <f aes>4. |
                   r8 <c d>8 \mf \< \( <aes' bes>-. \) \! |
                   <<
                     \new Voice \voiceOne {
                       aes'8 \f aes4 \< |
                       aes4 aes8 |
                       aes8 \ff aes4 |
                       aes4 \> aes8~ |
                       aes8 aes,4 |
                       aes4 aes8 \pp
                     }
                     \\
                     \new Voice \voiceTwo {
                       ees'8 \( aes, des |
                       ees aes,4 \) |
                       f'8 f4 |
                       f4 f8~ |
                       f8 f,4 |
                       f4 f8
                     }
                   >>
                   \bar "||"
                   \key a \major
                   
                   R4.^\markup{\bold "Plus lent"} |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   cis8^"1."^"solo"^\markup{\bold "a Tempo"} \p \( gis4\) |
                   R4. |
                   R4. |
                   R4.^\markup{\bold "Sans ralentir"} |
                   \bar "||"
                   \key des \major
                   R4. |
                   R4. |
                   << 
                     \new Voice \voiceOne 
                     {
                       f'8-- \pp f-- f-- |
                       c-- ees4-> |
                     }
                     \\
                     \new Voice \voiceTwo
                     {
                       des8 c bes |
                       f aes4 |
                     }
                   >>
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   \bar "||" 
                   \key e \major
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   r4 cis16 \p \( \< e \) |
                   gis8 \( fis16  gis e dis \) |
                   fis4 \! \( cis16 \> e |
                   gis4^\markup{\bold "Ralentissez beaucoup"} \) r8 \! |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   \bar "||"
                   \key des \major
                   R4.^\markup{\bold "Très lent"} |
                   R4. |
                   <<
                     \new Voice \voiceOne {
                       des'4. |
                       des4. \fermata  |
                     }
                     \\
                     \new Voice \voiceTwo {
                       ges,4 \f \> f8 |
                       ees8 f4 \p \fermata |
                     }
                   >>
                   \bar "||"
                  } 
                 
  % Pitches as written on a manuscript for Clarinet in A
  % are transposed to concert pitch
  hautboisMusic = \relative a'
                    { \key des \major
                      \time 3/8
                      des8^"1." \( \p aes'4 \) |
                      des,16 \( ges aes8. \) des16~-> \( |
                      des8 c16 des bes c | 
                      f,8 \) aes4-> |
                      des,4^"2." r8 |
                      c4 r8 |                      
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      r4 f16 \p \< \( aes \)  |
                      c8 \( bes16 c aes g \) \! |
                      bes8. \( f16 \> aes bes |
                      c8 \) r8 \!  r8 |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. | 
                      R4. |
                      R4. |
                      R4. |
                      ees,8^"1." \p \< \( aes, des |
                      c \! aes4 \> \) |
                      R4. \! |
                      <f' aes>8 \f <f aes>4 \< |
                      <f aes>4 <f aes>8 |
                      <f aes>8 \ff <f aes>4 |
                      <f aes>4 \> <f aes>8~ |
                      <f aes>8 \p r4  |
                      R4. |
                      \bar "||"
                      \key a \major
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      \bar "||"
                      \key des \major
                      <<
                        \new Voice \voiceOne {
                          des8 \p \( aes'4 \) |
                          des,16 \( ges aes8. \) des16~-> \( |
                          des8 c16 des bes c | 
                          f,8 \) aes4-> |
                          des,4 r8 |
                          ces4 r8 |
                        }
                        \\
                        \new Voice \voiceTwo {
                          des8 \( aes4 \) |
                          des8 \( aes4 \) |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                        }
                      >>
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      \bar "||" 
                      \key e \major
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      ais4.^"1." \pp |
                      a!-> |
                      ais |
                      a!-> |
                      ais4 r8 |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      \bar "||"
                      \key des \major
                      R4. |
                      r4 <c ees>8 \f |
                      <bes ges'>4 \> <aes f'>8  |
                      <ges ees'>8 <aes f'>4 \p \fermata |
                      \bar "||"
                    }
  
   corAnglaisMusic = \transpose c' f
                       \relative c'' 
                        { \key aes \major
                          \time 3/8
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          r8 r16 c'^"solo" \p \< \( ees f \) |
                          g8 \mf \( f16 g ees d \) |
                          f8 \>\( ees16 f des c~ \) |
                          c8 \! r8 r8 |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          \bar "||"
                          \key e \major
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          gis'8^"solo" \( \mp dis4 \) |
                          R4. |
                          R4. |
                          \bar "||"
                          \key aes \major
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          \bar "||" 
                          \key b \major
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          r4 gis,16 \mp \( b \) |
                          dis8 \( cis16 dis b ais \) |
                          cis8 \( b16 cis a gis \)
                          b8 \( a16 b gis fis~ \) |
                          fis4.~ \> |
                          fis4.~ |
                          fis4_\markup{\italic "morendo"} r8 \!
                          \bar "||"
                          \key des \major
                          R4. |
                          R4. |
                          R4.  |
                          R4. |
                          \bar "||"
                       }
                  
  clarinetMusic = \transpose c' bes
                  \relative a' 
                    { \key ees \major
                      \time 3/8
                      << 
                        \new Voice \voiceOne 
                        {
                          g8-- \p bes-- bes-- |
                          bes8-- bes-- bes-- |
                          bes-- bes-- bes-- |
                          f-- f4-> |
                        }
                        \\
                        \new Voice \voiceTwo
                        {
                          
                          ees8 f g |
                          aes g f |
                          g g g |
                          d d4 |
                        }
                      >>
                      R4. |
                      r4
                      << 
                        \new Voice \voiceOne 
                        {
                         a'8 \p \( |
                          g4 \) a8 \( |
                          bes4 \) a8 \( |
                          g4 \) g8 \( |
                          f4 \) ees8 |
                          ees8 \( d4 \) |
                          d4
                        }
                        \\
                        \new Voice \voiceTwo
                        {
                          
                          d8 \( |
                          ees4 \) g8 \( |
                          d4\) d8 \( |
                          ees4 \) c8 \( |
                          d4 \) bes8 \( |
                          c8 \) aes4 \( |
                          bes4 \) 
                        }
                      >>
                      r8 |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      << 
                        \new Voice \voiceOne {
                          bes'4 \p \< \( d8 |
                          des4 \! \> \) s8 |
                          bes4 \p \< \( d8 |
                          des4 \! \> \) s8 \! |
                        }
                        \\
                        \new Voice \voiceTwo {
                          e,4. \( |
                          f4 \) r8 |
                          e4. \( |
                          f4 \) r8 |
                        }
                      >>
                      
                      R4. |
                      R4. |
                      <<
                        \new Voice \voiceOne {
                          c'8^"solo" \p \< \( g'4 \) |
                          ees16 \( f g4 \) |
                          c8 \mp \>  \( bes16 c aes bes \) |
                          g8. \( f16 c8 \) |
                          bes4. \p | 
                          bes4. |
                          s8 e, \mp \< \( d'-. \) |
                          bes4. \p | 
                          bes4. |
                          s8 e, \mf \< \( d'-. \! \) |
                          bes8 \f \< bes4 |
                          bes4 bes8 |
                          bes8 \ff bes4 |
                          bes4 \> bes8~ |
                          bes8 bes4 |
                          bes4 bes8 \pp |
                        }
                        \\
                        \new Voice \voiceTwo {
                          R4. |
                          R4. |
                          R4. |
                          R4. |
                          g4. |
                          g4. |
                          r8 d\( c'-.\) |
                          g4. |
                          g4. |
                          r8 d\( c'-. \) |
                          g8 g4 |
                          g4 g8 |
                          g8 g4 |
                          g4 g8~ |
                          g8 g4 |
                          g4 g8 |
                        }
                      >>
                      \bar "||"
                      \key b \major
                      r8
                      <<
                        \new Voice \voiceOne {
                          ais4 \pp \( |
                          fis \) s8 |
                          fis4  \( fis8~ |
                          fis8 ais4 |
                          dis4.~ \> |
                          dis4_\markup{\italic "morendo"} \) s8 |
                          fis,4.^\markup{\bold "Reprenez peu à peu le mouvement"} \pp |
                          fis |
                          fis |
                          fis |
                        }
                        \\
                        \new Voice \voiceTwo {
                          fis4 |
                          dis r8 |
                          e4 dis8~ |
                          dis fis4 |
                          bis4.~ |
                          bis4 r8 |
                          e,4. |
                          e |
                          e |
                          e |
                          
                        }
                      >>
                      R4. |
                      R4. |
                      R4. |
                      dis'8^"1." \mp \> \( ais4 \) |
                      \bar "||"
                      \key ees \major
                      R4. \! |
                      R4. |
                      << 
                        \new Voice \voiceOne 
                        {                          
                          bes8-- \pp bes-- bes-- |
                          f-- f4-> |
                        }
                        \\
                        \new Voice \voiceTwo
                        {
                          g8 g g |
                          d d4 |
                        }
                      >>
                      R4. |
                      R4. |
                      aes'4^"1." \p \( g8 |
                      des'4 \) r8 |
                      aes4 \( ees8 \) |
                      d4 \( ees8 \) |
                      c4 \( bes8 \) |
                      bes4 r8|
                      \bar "||" 
                      \key fis \major
                      dis'8^"1 ."^\markup{\bold "Un peu plus lent qu'au début"} \p \( ais'4 \) |
                      fis16 \( gis ais4 \) |
                      dis8-. \( dis16-- dis-- cis-- dis-- \) |
                      b8 \( ais4 \) |
                      ais8\( \< gis16 ais fis eis |
                      gis4\) \mp dis16 \( \> fis |
                      ais8 \) \pp r4 |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      <<
                        \new Voice \voiceOne {
                          b,,4.~ \pp | 
                          b4.~ \> |
                          b4_\markup{\italic "morendo"} s8 \! |
                        }
                      \\
                        \new Voice \voiceTwo {
                          gis4.~ |
                          gis4.~
                          gis4 r8|
                        }
                      >>
                      \bar "||"
                      \key ees \major
                      <<
                        \new Voice \voiceOne {
                          g'4. \mf \< | 
                          g4 f8 |
                          aes4 \f \> g8  |
                          f8  g4 \p \fermata |
                        }
                      \\
                        \new Voice \voiceTwo {
                          ees4. |
                          ees4 d8 |
                          f4 ees8 |
                          ees8 ees4 |
                        }
                      >>
                      \bar "||"
                    }
                  
  bassonMusic = \relative c' 
                  { \clef bass
                    \key des \major
                    \time 3/8
                    R4. |
                    R4. |
                    des8--^"1." \p c-- bes-- |
                    f-- aes4-> |
                    R4. |
                    R4. |
                    aes4^"1." \p \( des8 |
                    c4 \) r8 |
                    R4. |
                    g4 \( c8 |
                    bes8 c bes|
                    aes4 \) r8 |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    bes4.^"1." \p \< \(  |
                    ces4 \! \> \) r8 \!  |
                    bes4. \p \< \( |
                    ces4 \>  \) r8 \! |
                    R4. |
                    R4. |
                    f8^"1." \p \< \( ees des |
                    bes des ees \) |
                    f8 \mp \> \( ees des |
                    bes des ees \) |
                    ees8 \!_\markup{ \dynamic p \italic dolce} \( aes, des |
                    c aes4 \> \) |
                    R4.\! |
                    <<
                      \new Voice \voiceOne {
                        ees'8^\p^\< \(  aes, des |
                        c\! aes4^\> \)  |
                        R4. \!  |
                        aes4.~  |
                        aes4. |
                        bes4.~ |
                        bes4.~ |
                        bes4.~ |
                        bes4. |
                      }
                      \\
                      \new Voice \voiceTwo {
                        des,4. \p \< \( |
                        f4. |
                        bes4.\) |
                        des,4. \f \< |
                        des4. |
                        bes4.~ \ff |
                        bes4.~ \> |
                        bes4.~ |
                        bes4. \! |
                      }
                    >>
                    \bar "||"
                    \key a \major
                    r8 d'4^"1." \pp \( |
                    ais \) r8 |
                    gis4 \( ais8~ |
                    ais8 d4 |
                    gis4.~ \> |
                    gis4_\markup{\italic "morendo"} \) r8 \! |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    \bar "||" 
                    \key des \major
                    R4. |
                    R4. |
                    des8--^"1." \pp c-- bes-- |
                    f-- aes4-> |
                    R4. |
                    R4. |
                    bes4^"1." \p \( aes8 \) |
                    f8 \( aes8 \) r8 |
                    bes4 \( aes8 \) |
                    ges4 \( aes8 \) |
                    bes8 \( ges4  |
                    f4 \) r8 |
                    \bar "||" 
                    \key e \major
                    R4. |
                    R4. |
                    R4. |
                    R4. |
                    <<
                      \new Voice \voiceOne {
                        cis'4. \pp |
                        bis4.-> |
                        cis4. |
                        bis4.-> |
                        cis4. |
                        bis4~ bis16 r16 |
                        bis4~ bis16r16 |
                      }
                      \\
                      \new Voice \voiceTwo {
                        ais4. |
                        a! |
                        ais |
                        a! |
                        ais |
                        a!4~ a16 s16 |
                        a!4~ a16 s16 |
                        
                      }
                    >>
                    R4. |
                    R4. |
                    R4. |
                    \bar "||"
                    \key des \major
                    <<
                      \new Voice \voiceOne {
                          aes4 \mf \< f8~ |
                          f8 c4 |
                          ees'4 \f \> des8 |
                          des8 des4 \p \fermata |
                      }
                      \\
                      \new Voice \voiceTwo {
                        des,4 bes8~ |
                        bes8 f4 |
                        des'4.~ |
                        des4. \fermata |
                      }
                    >>
                    \bar "||"
                  }                  

% Key signature is often omitted for horns
  hornundeuxMusic = \transpose c' f
                     \relative c' 
                       {
                         \time 3/8
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         ees4-+^"1." \p \( aes8-+ |
                         g4-+ \) r8 |
                         R4. |
                         R4. |
                         <<
                           \new Voice \voiceOne 
                           {
                             R4. |
                             r4 c8~^\markup{\dynamic "pp"} |
                             c4 r8 |
                             R4. |
                             R4. |
                             
                           }
                           \\
                           \new Voice \voiceTwo 
                           {
                             r4 c,8~ \pp |
                             c4.~ |
                             c8 r8 c8~ \(|
                             c4. |
                             f8 \) \> r8  r8 \! |
                            
                           }
                         >>
                         r8
                          <<
                           \new Voice \voiceOne 
                           {
                             c'4-> \p |
                           }
                           \\
                           \new Voice \voiceTwo 
                           {
                              c,4 |
                                                        
                           }
                         >>
                         R4. |
                         r8
                         <<
                           \new Voice \voiceOne 
                           {
                             c'4-> \p |
                            
                           }
                           \\
                           \new Voice \voiceTwo 
                           {
                             c,4 |                             
                             
                           }
                         >>
                         R4. |
                         <<
                           \new Voice \voiceOne 
                           {
                             
                             bes'8 \p \> \( a g |
                             f4. \pp \) |
                           }
                           \\
                           \new Voice \voiceTwo 
                           {
                             ges8 \( f ees |
                             des4 \) r8 |                                                        
                           }
                         >>
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         r8 <c ees>8--  \f \< <ees aes>8-- |
                         <aes c>8-- <c ees>8--  \! r8 |
                         r8 <ees, g>8--  \ff  <g c>8-- |
                         <c ees>8-- \> <ees g>8--  r8 \! |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         <a, b>4.^"+" \pp \< |
                         <a bis>4.^"+" |
                         <ais cis>4.^"+" \mp \> |
                         <b cisis>4.^"+" |
                         \bar "||"
                         r8 \! <ges ces>4 \pp |
                         r8 <ges ces>4 |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                       <<
                         \new Voice \voiceOne {
                           r4 gis,8~ \p |
                           gis4 r8 |
                           r4 gis8~ |
                           gis4 r8 |
                         }
                         \\
                         \new Voice \voiceTwo {
                           R4. |
                           r4 gis8~ \p |
                           gis4 r8 |
                           R4. |
                         }
                       >>
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         R4. |
                         r4 <fisis a>8~ \pp |
                         <fisis a>4.~ |
                         <fisis a>4-\markup{\italic "morendo"} r8 |
                         \bar "||"
                          r8
                          << 
                            \new Voice \voiceOne {
                              c''4 \mf |
                              c4  \< bes8 |
                              aes4 \f \> g8 |
                              f8 ees4 \p \fermata |
                            }
                            \\
                            \new Voice \voiceTwo {
                               aes4 |
                               aes4 ees8 |
                               aes,4.  |
                               aes4. \fermata |
                            }
                          >>
                          \bar "||"
                       }
                       
                          
  trumpetMusic = \relative c''
                   { 
                     \time 3/8
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     ees8 \f \( aes, des \) |
                     ees-- aes,4-- |
                     R4.^"Mettre la sourdine" |
                     R4. |
                     R4. |
                     R4. |
                     r8^"(sourdine)" cis4\( ( \p |
                     gis4. ) |
                     e8(  fis) gis8~-- |
                     gis8 cis4-- |
                     e4.~--  \> |
                     e4_\markup{\italic "morendo"} \) r8 \! |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     cis8 \( \mp \> gis4 \) |
                     R4. \! |
                     \bar "||"
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     cis4. \pp |
                     bis4.-> |
                     cis4. |
                     bis 4.-> |
                     cis4 r8 |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     R4.^"Otez la sourdine" |
                     aes4 \mf \< aes8 |
                     bes4 \f \> aes8   |
                     ges8  f4 \fermata \p |
                          \bar "||"
                   }
                   
  celestaMusicMD = \relative c' 
                   { 
                     \key des \major
                     \time 3/8
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     \key a \major
                     cis''8 \p \( gis e16 fis  |
                     gis8 cis e \) |
                     cis8 \( gis e16 fis  |
                     gis8  cis e \) |
                     cis \( gis e |
                     cis gis e \) |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     \key des \major
                     R4. |                     
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||" 
                     \key e \major
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
\bar "||"
                          \key des \major
                          R4. |
                          R4. |
                          R4.  |
                          R4. |
                          \bar "||"
                   }                   
  
   celestaMusicMG = \relative c' 
                   { 
                     \key des \major
                     \time 3/8
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     \key a \major
                     fis'8  cis r  |
                     cis8 r r |
                     gis' r r  |
                     cis, fis r |
                     gis r r |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     \key des \major
                     R4. |                     
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||" 
                     \key e \major
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
\bar "||"
                          \key des \major
                          R4. |
                          R4. |
                          R4.  |
                          R4. |
                          \bar "||"
                   }
                    
  harpeMusicMD = \relative c'' 
                   { 
                     \key des \major
                     \time 3/8
                     R4.^\markup{"Si"\flat" Do"\sharp " Ré | Mi Fa Sol La" \flat}  |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                   \bar ":|."
                     \grace { g''16} <f, f'>4. \p  |
                     \grace { e'16 } d4. |
                     \grace { ges,16 bes16 } f'4._\markup{"Sol"\flat} |
                     \grace { e,16_\markup{"Ré"\sharp } } aes8  \grace { dis,16_\markup{"Sol" \natural} } g8  \grace {dis16_\markup{"Fa" \sharp}} fis8 |
                     <d f>4. \arpeggio |
                     <ces ees> \mp \arpeggio |
                     <d f>4. \pp \arpeggio |
                     <ces ees> \mp \arpeggio | |
                     <d f>4. \arpeggio  |
                     R4. |
                     <bes, f'>4.~ \p \arpeggio  |
                     <bes f'>4. |
                     <bes' f'>4. \arpeggio  |
                     ges4. \arpeggio |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     r8 \f \< <f, aes des f> \arpeggio <aes des f aes> \arpeggio |
                     <des f aes des> \arpeggio <f aes des f> \arpeggio \laissezVibrer r8 |
                     r8 \ff <f, aes c f> \arpeggio <aes c f aes> \arpeggio |
                     <c f aes c> \arpeggio \> <f aes c f> \arpeggio \laissezVibrer r8
                     <aes c f aes> \arpeggio \laissezVibrer r <f aes c f> \arpeggio |
                     r <c f aes c> \arpeggio r \!|
                     \bar "||"
                     \key e \major
                     r8 \p <d e gis>4 \arpeggio |
                     <ais cis e> \arpeggio r8 |
                     <gis d' e>4 \arpeggio <ais cis e>8~ \arpeggio |
                     <ais cis e>8 <d e gis>4 \arpeggio |
                     <gis ais cis>4 \arpeggio <e' gis cis e>8~ \arpeggio |
                     <e gis cis e>8 <gis, cis e gis>4 |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||" 
                     \key des \major
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     
                     \key e \major
                     %\grace { dis''16 } cis4. \p  |
                     cis'4. \p  |
                     \grace { bis16 } ais4. |
                     \grace { d,16 fis16 } cis'4. |
                     \grace { b,16 d } a'8 g4 |
                     r4  \tuplet 3/2 {cis,,16 cis' cis,} |
                     \tuplet 3/2 {cis'16 des cis} \tuplet 3/2 {cis,16 cis' cis,} r8 |
                     r4  \tuplet 3/2 {cis16 cis' cis,} |
                     \tuplet 3/2 {cis'16 des cis} \tuplet 3/2 {cis,16 cis' cis,} r8 |
                     r4  \tuplet 3/2 {cis16 cis' cis,} |
                     \tuplet 3/2 {cis'16 des cis} \tuplet 3/2 {cis,16 cis' cis,} r8 |
                     r4  \tuplet 3/2 {cis16 cis' cis,} |
                     \tuplet 3/2 {cis'16 des cis} \tuplet 3/2 {cis,16 cis' cis,} r8 |
                     R4. |
                     R4. |
                     \bar "||"
                     \key des \major
                     r8 <des f>4 |
                     <des f>4 <aes c ees aes>8 |
                     <des ees ges bes>4 \arpeggio <c des f aes>8  |
                     <bes des ees ges>8 <aes des f aes>4 \fermata |
                     \bar "||"
                   }
  
  harpeMusicMG = \relative c'
                   { 
                     \clef bass
                     \key des \major  
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. \clef treble|
                     <bes d aes'>4._\markup{"Ré"}_\markup{"Fa"} \arpeggio |
                     <ces ees>_\markup{"Mi" \flat} \arpeggio |
                     <bes d aes'>4. \arpeggio |
                     <ces ees>  \arpeggio | |
                     <bes d aes'>4. \arpeggio  |
                     R4. \clef bass|
                     <ees,, bes' ges'>4.~ \arpeggio |
                     <ees bes' ges'>4. |
                     <ees' bes' f'>4. \arpeggio |
                     <aes, ees' bes'>4. \arpeggio |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     <des, aes'>4.~ |
                     <des aes'>4. |
                     <bes bes'>4.~ |
                     <bes bes'>4.~ |
                     <bes bes'>4.~ |
                     <bes bes'>4. |
                     \bar "||"
                     \key a \major
                     e4.~ |
                     e4. |
                     e4.~ |
                     e4. |
                     e4.~ |
                     e4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \bar "||"
                     \key des \major 
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     
                     \bar "||" 
                     \key e \major
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     \tuplet 3/2 {fis16 cis' fis,} \tuplet 3/2 {cis' cis' cis,} r8  |
                     r8  r8 \tuplet 3/2 {cis16 cis' cis,}|
                     \tuplet 3/2 {fis,16 cis' fis,} \tuplet 3/2 {cis' cis' cis,} r8  |
                     r8  r8 \tuplet 3/2 {cis16 cis' cis,}|
                     \tuplet 3/2 {fis,16 cis' fis,} \tuplet 3/2 {cis' cis' cis,} r8  |
                     r8  r8 \tuplet 3/2 {cis16 cis' cis,}|
                     \tuplet 3/2 {fis,16 cis' fis,} \tuplet 3/2 {cis' cis' cis,} r8  |
                     r8  r8 \tuplet 3/2 {cis16 cis' cis,}|
                     fis,8 cis8 fis,8 \laissezVibrer |
                     r4 <des des'>8~ |
\bar "||"
                          \key des \major
                          <des des'>4^\mf^\< \laissezVibrer <bes' bes'>8~ |
                          <bes bes'>8 <f f'>4 |
                          <des des' aes'>4.~^\f^\> \arpeggio   |
                          <des des' aes'>4~ <des des' aes'>8^\p \fermata|
                          \bar "||"
                   }
                    
  violinIMusic = \relative c'' 
                   { 
                     \key des \major
                     \time 3/8
                     des8^"Pizz"^\markup{\bold "Mouvement de menuet"} \p des des |
                     des des des |
                     des c bes |
                     f aes4 |
                     
                     bes'8^"Arco" \mp \( f \) aes~-> \( |
                     aes f \) c~-> \( |
                     c bes16 c des8 \) |
                     g \( f \) c~-> \( |
                     c bes16 c g8~ |
                     g \) aes16 \( bes aes g \)  |
                     f \( g \)  ees4-> |
                     f r8 |
                     <<
                       \new Voice \voiceOne {
                         <f bes\harmonic>4.^"div." \pp |
                         <d g\harmonic>4.  |
                         bes''4. |
                         aes8 \( g ges \)  |
                         f4:32 \pp \< r8 |
                         ees4:32 \mp \> r8 |
                         f4:32 \pp \< r8 |
                         ees4:32 \mp \> r8 |
                         f4.:32 \p \> |
                         ees8^"Pizz" d8 \! 
                       }
                       \\
                       \new Voice \voiceTwo {
                         f4. |
                         aes4. |
                         ges4. |
                         fes8 \( ees4 \) |
                         d4:32 s8 |
                         ees4:32 s8 |
                         d4:32 s8 |
                         ees4:32 s8 |
                         d4.:32 |
                         ees8 d8 
                       }
                     >>
                     r8|
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     R4. |
                     r8
                     <<
                       \new Voice \voiceOne {
                         d,8:32^"Arco div." \p \< bes'16:32 r16 \!  |
                       }
                       \\
                       \new Voice \voiceOne {
                         c,8:32 aes'16:32 s16 |
                       }
                     >>
                     R4. |
                     R4. |
                     r8
                     <<
                       \new Voice \voiceOne {
                          d8:32 \mf \< bes'16:32 r16 \! |
                       }
                       \\
                       \new Voice \voiceOne {
                          c,8:32 aes'16:32 s16 |
                       }
                     >>
                     
                     aes16 \( \downbow \f bes \) \< c8. \( des16 \)  |
                     ees8. \( f16 \) g \( aes \) |
                     c \ff aes c8. aes16 |
                     c8. \> aes16 c8~ |
                     c16 aes c8. aes16 |
                     c8. aes16 c \( aes \) \pp |
                     \bar "||"
                     \key a \major
                     cis,8^"Solo"^\markup{\bold "Plus lent"} \downbow \p \( gis e16 fis \) |
                     gis8 \( cis e \) |
                     cis8 \( gis\) e16 \( fis \) |
                     gis8 \( cis e \) |
                     cis \( gis e \) |
                     cis \( gis e \) |
                     cis'^"Tutti"^\markup{\bold "Reprenez peu à peu le mouvement"} \pp \( gis4 \) |
                     e16 \( fis gis8 cis16 e \) |
                     cis8 \> r8 \! r8 |
                     R4. |
                     e,4.:32^\markup{\bold "a Tempo"} \pp \< |
                     eis:32 |
                     fis:32 \mp \> |
                     fisis:32^\markup{\bold "Sans ralentir"} |
                     \bar "||"
                     \key des \major
                     aes8 \p \( fes des \) |
                     aes'8 \( fes des \) |
                     f'8^"Pizz." f f |
                     c ees4^"Arco" |
                     bes'8 \mp \( f \) aes~-> \( |
                     aes f \) des~-> \( |
                     des bes16 c des8 \) |
                     aes' \( f \) des~-> \( |
                     des bes16 c f,8~ |
                     f \) ges16 \( aes ges f \)  |
                     ees \( f \)  c4-> |
                     des r8 |
                     \bar "||" 
                     \key e \major
                     <<
                       \new Voice \voiceOne {
                         cis''4.^"div."^\markup{\bold "Un peu plus lent qu'au début"} \pp |
                         ais4.  |
                         fis4. |
                         d4~ d16 
                       }
                       \\
                       \new Voice \voiceTwo {
                         cis4. |
                         e4. |
                         d4. |
                         b4~ b16 
                       }
                       >>
                       r16
                       R4. |
                     <<
                       \new Voice \voiceOne {
                         cis32 \( cis' cis, cis' cis,8 \) r8 |
                       }
                       \\
                       \new Voice \voiceTwo { 
                         cis4  s8 |
                       }
                     >>
                       R4. |
                     <<
                       \new Voice \voiceOne {
                         cis32 \( cis' cis, cis' cis,8 \) r8 |
                       }
                       \\
                       \new Voice \voiceTwo { 
                         cis4  s8 |
                       }
                     >>
                     R4.^\markup{\bold "Ralentissez beaucoup"} |
                     <<
                       \new Voice \voiceOne {
                         cis32 \( cis' cis, cis' cis,8 \) r8 |
                       }
                       \\
                       \new Voice \voiceTwo { 
                         cis4  s8 |
                       }
                     >>
                     R4. |
                     cis8 \( cis,4~ \)  |
                     cis4.~ |
                     cis4 r8 |
                     \bar "||"
                     \key des \major
                     <<
                       \new Voice {
                         aes'16^\markup{\bold "Très lent"} \( bes \) c8. \( des16 \) |
                         ees8. \( f16 \) g \( aes \) |
                         des4. |
                         des4. \fermata |
                       }
                       \\
                       \new Voice {
                         s4. \mf \< |
                         s4. |
                         s4.\f \> |
                         s4 s8 \p |
                       }
                     >>
                     \bar "||"
                   }
                   
  violinIIMusic = \relative c'' 
                    { 
                      \key des \major
                      \time 3/8
                      
                      aes8^"Pizz." \p bes c |
                      bes c bes |
                      f' f f |
                      c ees4 |
                      
                      R4. |
                      r4 aes,8~^"Arco" \p |
                      aes4 r8 |
                      r4 aes8~ |
                      aes4 g8~ |
                      g4 f8~ |
                      f16 g ees4 |
                      f4 r8 |
                      <<
                        \new Voice \voiceOne {
                          ees'4.^"div." \pp
                          d |
                          c |
                          bes8 \( a4 \) |
                          c4:32 \pp \< r8 |
                          ces4:32 \mp \> r8 |
                          c!4:32 \pp \< r8 |
                          ces4:32 \mp \> r8 |
                          c!4.:32 \p \> |
                          ces8^"Pizz" bes8 \! 
                        }
                        \\
                        \new Voice \voiceTwo {
                          aes4.
                          bes |
                          aes |
                          ges8 \( f ees \) |
                          aes4:32 s8 |
                          ces4:32 s8 |
                          aes4:32 s8 |
                          ces4:32 s8 |
                          aes4.:32  |
                          ces8 bes8 
                        }
                      >>
                      r8 |
                      R4. |
                      R4. |
                      R4. |
                      ges4^"Arco" \p \> ges8 |
                      f8 \pp r8 r8 |
                      R4. |
                      r8 <c d>8^"Pizz" \mp \< <aes' bes>  |
                      R4. \! |
                      R4. |
                      r8 <c d>8 \mf \< <aes' bes>  \!|
                      <<
                        \new Voice \voiceOne {
                          aes,16^"Arco div." \downbow \( \f bes \) \< c8. \( des16 \)  |
                          ees8. \( f16 \) g  \( aes \) |
                          c \ff aes c8. aes16 |
                          c8. \> aes16 c8~ |
                          c16 aes c8. aes16 |
                          c8. aes16 c \( aes \) \! |
                        }
                        \\
                        \new Voice \voiceTwo {
                          aes,16:32 bes:32 c8.:32 des16:32  |
                          ees8.:32 f16:32 g:32 aes:32 |
                          c:32 aes:32 c8.:32 aes16:32 |
                          c8.:32 aes16:32 c8~:32 |
                          c16 aes c8.:16 aes16 |
                          c8. aes16 c aes \pp|
                        }
                      >>
                      \bar "||"
                      \key a \major
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      cis,,8 \pp \( b gis \) |
                      cis \> \( gis4 \) |
                      cis8 \pp \( b gis \) |
                      cis \> \( gis4 \) |
                      d'4.:32 \pp \<|
                      d4.:32 |
                      dis:32 \mp \>|
                      e:32 |
                      \bar "||"
                      \key des \major
                      f8 \p \( aes4 \) |
                      f8 \( aes4 \) |
                      des8^"Pizz" c bes |
                      f aes4^"Arco" |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      R4. |
                      \bar "||" 
                      \key e \major
                      <<
                        \new Voice \voiceOne {
                          b4.\pp |
                          ais4. |
                          gis4. |
                          eis4~ eis16 
                        }
                        \\
                        \new Voice \voiceTwo {
                          e4. |
                          fis4. |
                          e4. |
                          d4~ d16 
                        }
                      >>
                      r16 |
                      r4 cis32 \( cis' cis, cis' |
                      \autoBeamOff
                      cis,8 \)  \autoBeamOn cis32 \( cis' cis, cis' cis,8 \) |
                      r4 cis32 \( cis' cis, cis' |
                      \autoBeamOff
                      cis,8 \) \autoBeamOn cis32 \( cis' cis, cis' cis,8 \) |
                      r4 cis32 \( cis' cis, cis' |
                      \autoBeamOff
                      cis,8 \) \autoBeamOn cis32 \( cis' cis, cis' cis,8 \) |
                      r4 cis8 \( |
                      cis'4. \) |
                      R4. |
                      R4. |
                     \bar "||"
                     \key des \major
                     <<
                       \new Voice {
                         aes16 \( bes \) c8. \( des16 \) |
                         ees8. \( f16 \) g \( aes \) |
                         des4. |
                         des4. \fermata |
                       }
                       \\
                       \new Voice {
                         s4. \mf \< |
                         s4. |
                         s4.\f \> |
                         s4 s8 \p |
                       }
                     >>
                     \bar "||"
                    }
                    
  violaMusic = \relative c' 
                 { 
                   \clef alto 
                   \key des \major
                   \time 3/8
                   
                   f8^"Pizz" \p aes aes |
                   aes aes aes |
                   <f aes> <f aes> <f aes> |
                   <c ees> <c ees>4 |
                   
                   bes'8^"Arco" \mp \( f \) aes~-> \( |
                   aes f \) c~-> \( |
                   c bes16 c des8 \) |
                   g \( f \) c~-> \( |
                   c bes16 c g8~ |
                   g \) aes16 \( bes aes g \)  |
                   f \( g \)  ees4-> |
                   f r8 |
                   <<
                     \new Voice \voiceOne {
                       f'4.^"div" \pp |
                       f4. |
                       ees |
                       des8 \( c a \) |
                       d4:32 \pp \< r8 |
                       ees4:32 \mp \> r8 |
                       d4:32 \pp \< r8 |
                       ees4:32 \mp \> r8 |
                       d4.:32 \p \> |
                       ees16.:32[ r32 d16.:32] \! 
                     }
                     \\
                     \new Voice \voiceTwo {
                       R4. |
                       r4 f8_"Pizz." |
                       R4. |
                       R4. |
                       bes,4:32 s8 |
                       ces4:32 s8 |
                       bes4:32 s8 |
                       ces4:32 s8 |
                       bes4.:32 |
                       ces16.:32[ r32 bes16.:32] 
                     }
                   >>
                   r32 r8 |
                   ges4.^"uni" \pp \< |
                   ges'4. |
                   f8 \p  \> r8 r8 |
                   bes,4 c8 |
                   des8 \pp r8 r8 |
                   R4. |
                   R4. |
                   aes16^\markup{ \italic "en dehors"} \( \p bes \< c8. des16 \)  |
                   ees8. \( f16 \) aes \( c \) |
                   bes \( aes \) f \( d \) bes8 \! |
                   <<
                     \new Voice \voiceOne {
                        aes16^"div." \( \f bes \) \< c8. \( des16 \)  |
                        ees8. \( f16 \) g \( aes \) |
                        c \ff aes c8. aes16 |
                        c8. \> aes16 c8~ |
                        c16 aes c8. aes16 |
                        c8. aes16 c \( aes \) \pp |
                      }
                      \\
                      \new Voice \voiceTwo {
                        aes,16:32 \( bes:32 c8.:32 des16:32 \)  |
                        ees8.:32 f16:32 \( g:32 aes:32 \) |
                        c:32 aes:32 c8.:32 aes16:32 |
                        c8.:32 aes16:32 c8~:32 |
                        c16 aes c8.:16 aes16 |
                        c8. aes16 \( c aes \) |
                      }
                   >>
                   \bar "||"
                   \key a \major
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   cis8 \pp \( gis4 \) |
                   e16 \( fis gis8 cis16 e \) |
                   cis,8 \< \( b gis \) |
                   cis8 \( b gis \) |
                   cis8 \mp \> \( b gis \) |
                   cis8 \( b gis \) |
                   \bar "||" 
                   \key des \major
                   des' \p \( ces aes \) |
                   des \( ces aes \) |
                   <f' aes>8^"Pizz." <f aes> <f aes> |
                   <c ees> <c ees>4^"Arco" |
                   bes'8 \mp \( f \) aes~-> \( |
                   aes f \) des~-> \( |
                   des bes16 c des8 \) |
                   aes' \( f \) des~-> \( |
                   des bes16 c f,8~ |
                   f \) ges16 \( aes ges f \)  |
                   ees \( f \)  c4-> |
                   des r8 |
                   \bar "||" 
                   \key e \major
                   cis'4.  \pp |
                   cis |
                   b |
                   gis4~ gis16 r16 |
                   r8 cis,32\( cis' cis, cis' cis,8 \) |
                   r8  r8 cis32 \( cis' cis, cis'  |
                   \autoBeamOff
                   cis,8 \) \autoBeamOn cis32\( cis' cis, cis' cis,8 \) |
                   r8 r8 cis32 \( cis' cis, cis' | 
                   \autoBeamOff
                   cis,8 \) \autoBeamOn cis32\( cis' cis, cis' cis,8 \) |
                   r8 r8 cis32 \( cis' cis, cis' | 
                   \autoBeamOff
                   cis,8 \) \autoBeamOn cis8 \( cis'8 \) |
                   r8 cis8 \( cis,8~ \) |
                   cis4.~ |
                   cis4 r8 |
                   \bar "||"
                   \key des \major
                     <<
                       \new Voice {
                         aes'16 \( bes \) c8. \( des16 \) |
                         ees8. \( f16 \) g \( aes \) |
                         des4. |
                         des4. \fermata |
                       }
                       \\
                       \new Voice {
                         s4. \mf \< |
                         s4. |
                         s4.\f \> |
                         s4 s8 \p |
                       }
                     >>
                   \bar "||"
                 }
                 
  celloMusic = \relative c, 
                 { 
                   \clef bass 
                   \key des \major
                   \time 3/8
                   
                   des''8^"Pizz" \p ees f |
                   ges f ees |
                   des c bes |
                   f aes4 |
                   R4. |
                   
                   <des,, aes'>4^"Arco div." \p <f c'>8 |
                   <bes f'>4 <ees bes'>8 |
                   <f, c'>4. |
                   <bes f'>4 \upbow <ees bes'>8 |
                   <aes, ees'>4 <des aes'>8 |
                   g,8 <c g'>4 |
                   r8 c^"Pizz." f, |
                   r4 f'8 \pp \laissezVibrer |
                   r4.  |
                   r4 f8 \laissezVibrer  |
                   r4. |
                   r8 f \< bes |
                   r8 f'4-> \! |
                   r8 f, \pp \< bes |
                   r8 f'4-> \! |
                   r8 f,, \pp \< bes |
                   f'4^"Arco" \p \( bes,8 \) |
                   bes4.~ \pp \< |
                   bes4. |
                   bes'4. \p \> |
                   ees,4 r8 |
                   aes,16^\markup{\italic "en dehors"} \( \p bes \< c8. des16 \)  |
                   ees8. \( f16 \) aes \( c \) |
                   bes \( aes \) f \( d \) bes8 |
                   aes16 \( \p bes \< c8. des16 \)  |
                   ees8. \( f16 \) aes \( c \) |
                   bes \( aes \) f \( d \) bes8 |
                   <des, aes'>4.~ \f \< |
                   <des aes'>4. |
                   bes'4.~ \ff  |
                   bes4.~ \> |
                   bes4.~ |
                   bes4. \pp|
                   \bar "||"
                   \key a \major
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   \bar "||" 
                   \key des \major
                   R4. |
                   R4. |
                   des'8^"(Pizz.)" c bes |
                   f aes4^"Arco" |
                   R4. |
                   <des,, aes'>4.^"div." \p |
                   <ges des'>4 \( <bes f'>8 \) |
                   <des, aes'>4. |
                   <ges des'>4 \( <bes aes'>8 \) |
                   <aes ges'>4 \( <bes aes'>8 \) |
                   <ges des'>8 \( <aes ges'>4 \) |
                   r8 aes8^"Pizz" des, |
                   \bar "||" 
                   \key e \major
                   r4 cis'8 \pp \laissezVibrer |
                   r4 cis8 \laissezVibrer |
                   r4 cis8 \laissezVibrer |
                   r4 cis8^"Arco" |
                   fis,32 \( cis' fis, cis' fis,4 \) |
                   <cis' fis\harmonic>8 \( cis' cis, \) |
                   fis,32 \( cis' fis, cis' fis,4 \)  |
                   <cis' fis\harmonic>8 \( cis' cis, \) |
                   fis,32 \( cis' fis, cis' fis,4 \)  |
                   <cis' fis\harmonic>8 \( cis' cis, \) |
                   fis8 \( cis'8 \) r8 |
                   <<
                     \new Voice \voiceOne {
                       r4 cis,8^"div." \( |
                       fis,4.~ \) |
                       fis4 r8 |
                       \key des \major
                       des'4 \mf \< f8~ |
                       f8 c4 |
                       aes4.~ \f \> |
                       aes4 aes8  \fermata \p |
                     }
                     \\
                     \new Voice \voiceTwo {
                       R4. |
                       fis8 \( cis4~ \) |
                       cis4 des8~^"div." |
                       des4 bes'8~ |
                       bes8 f4 |
                       des4.~ |
                       des4 des8 |
                     }
                   >>
                   
                   \bar "||" 
                }	
                 
                 
  bassMusic = \relative c, 
                { 
                  \clef "bass_8" 
                  \key des \major
                   \time 3/8
                   R4. |
                   R4. |
                   R4. |
                   r4 f8^"Pizz." \pp |
                   bes,4 \laissezVibrer r8 |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   r8 c8  \pp f,8 | 
                   R4. |
                   R4. |
                   R4. |
                   R4. |                   
                   r8 f' \pp \< bes |
                   r8 f'4-> \! |
                   r8 f, \pp \< bes |
                   r8 f'4-> \! |
                   R4. |
                   R4. |
                   ees,4.~ \pp \< |
                   ees4. |
                   ees'4. \p \> |
                   aes,4 r8 |
                   des,4. \ppp \<  |
                   f4. |
                   bes |
                   des,4. \p \<  |
                   f4. |
                   bes |
                   <des, aes'>4.~ \f \<  |
                   <des aes'>4. |
                   bes~ \ff |
                   bes~  \>|
                   bes~ |
                   bes  |
                   \bar "||"
                   \key a \major
                   e4.~ \pp |
                   e4. |
                   e4.~ |
                   e4. |
                   e4.~ |
                   e8 r8 r8 |
                   r4. |
                   r8 r8 e^"Pizz" \laissezVibrer |
                   r4. |
                   r8 r8 e \laissezVibrer |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   \bar "||" 
                   \key des \major
                   R4. |
                   R4. |
                   R4. |
                   r4 f8^"(Pizz.)" \pp |
                   bes,4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   r8 aes'8 des,8 |
                   \bar "||" 
                   \key e \major
                   R4. |
                   R4. |
                   R4. |
                   R4. |
                   fis8^"Arco" \( cis' <a, fis'\harmonic> \) |
                   R4. |
                   fis'8 \( cis' <a, fis'\harmonic> \) |
                   R4. |
                   fis'8 \( cis' <a, fis'\harmonic> \) |
                   R4. |
                   fis'4.~ |
                   fis4. |
                   <<
                     \new Voice \voiceOne {
                       r8 cis8 \pp \( fis,8~ \) |
                       fis4 des'8~^"4 c." |
                       des4 \mf \< s8 |
                       s4. |
                       des4.~ \f \> |
                       des4 des8 \fermata \p |
                     }
                     \\
                     \new Voice \voiceTwo {
                       s4. |
                       s4 des,8~_"5 c." |
                       \bar "||"
                       \key des \major
                       des4 bes'8 |
                       bes8 f4 |
                       des4.~ |
                       des4 des8 |
                     }
                   >>
                   \bar "||" 
                }	

\header	{
  title = "Sonatine"
  subtitle = "II"
  composer = "M. Ravel"
  arranger = "arr : L. Feisthauer"
}

\score {
    <<
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new Staff = "Staff_flute" \with {
          instrumentName = #"Flûtes"
          shortInstrumentName =#"Flt."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \fluteMusic
        }
        \new GrandStaff = "GrandStaff_hautbois" <<
        \new Staff = "Staff_hautbois" \with {
          instrumentName = #"Hautbois"
          shortInstrumentName = #"Hb."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \hautboisMusic
        }
        \new Staff = "Staff_corAnglais" \with {
          instrumentName = #"Cor Anglais"
          shortInstrumentName = #"C.A."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \transposition f
          \transpose f c
          \corAnglaisMusic
        }
        >>
        \new Staff = "Staff_clarinet" \with {
          instrumentName = \markup { \right-column { "Clarinettes" \line { "en Si" \flat } }}
          shortInstrumentName = \markup { \center-column { "Clar." \line {"Si" \flat}}}
        }
        {
          % Declare that written Middle C in the music
          % to follow sounds a concert B flat, for
          % output using sounded pitches such as MIDI.
            %\transposition a
          % Print music for a B-flat clarinet
            \transpose bes c' 
            \clarinetMusic
        }
        \new Staff = "Staff_basson" \with {
          instrumentName = #"Bassons"
          shortInstrumentName = #"Bsn."
        }
        {
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \bassonMusic
        }
      >>
      \new StaffGroup = "StaffGroup_brass" <<
          \new Staff = "Staff_horn12" \with{
            instrumentName = \markup{ \right-column{"Cors I & II" \line {"en Fa"}}}
            shortInstrumentName = \markup{ \center-column{"Cors" \line {"Fa"}}}
          }
          {
            \transposition f
            \transpose f c' \hornundeuxMusic
          }
        \new Staff = "Staff_trumpet" \with{
          instrumentName = \markup{ \right-column{"Trompette"}}
          shortInstrumentName = \markup{ \center-column{"Tpt." }}
        }
        {
          \trumpetMusic
        }
        
      >>  
      
        \new PianoStaff = "Staff_celesta" \with{
          instrumentName = #"Celesta"
          shortInstrumentName = #"Celesta"
        } 
        <<
          \set PianoStaff.connectArpeggios = ##t
          \new Staff = "CRH"
          <<
            \celestaMusicMD
          >>
          \new Staff = "CLH"
          <<
            \celestaMusicMG
          >>
        >>
      
      
      \new PianoStaff = "Staff_harp" \with{
          instrumentName = #"Harpe"
          shortInstrumentName = #"Harpe"
        } 
        <<
          \set PianoStaff.connectArpeggios = ##t
          \new Staff = "RH"
          <<
            \harpeMusicMD
          >>
          \new Staff = "LH"
          <<
            \harpeMusicMG
          >>
        >>
      \new StaffGroup = "StaffGroup_strings" \with {
        \override VerticalAxisGroup.remove-empty = ##f
        }<<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" \with{
            \override VerticalAxisGroup.remove-empty = ##f
            instrumentName = #"Violons I"
            shortInstrumentName = #"Vlns I"
          }
          {
            \violinIMusic
          }
          \new Staff = "Staff_violinII" \with{
            \override VerticalAxisGroup.remove-empty = ##f
            instrumentName = #"Violons II"
            shortInstrumentName = #"Vlns II"
          }
          {
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" \with{
          \override VerticalAxisGroup.remove-empty = ##f
          instrumentName = #"Altos"
          shortInstrumentName = #"Altos"
        }
        {
          \violaMusic
        }
        \new Staff = "Staff_cello" \with{
          \override VerticalAxisGroup.remove-empty = ##f
          instrumentName = #"Violoncelles"
          shortInstrumentName = #"Vlcs"
        }
        {
          \celloMusic
        }
        \new Staff = "Staff_bass" \with{
          instrumentName = #"Contrebasses"
          shortInstrumentName = #"Cbs"
        }
        {
          \bassMusic
        }
      >>
>>

\layout {
   #(layout-set-staff-size 15)
   \context {
     \Staff
     \RemoveEmptyStaves
   }
  }


  
}