\version  "2.18.2"

%#(set-global-staff-size 17)
\paper {
  indent = 3.0\cm % space for instrumentName
  short-indent = 1.5\cm % space for shortInstrumentName
}

  %bois
  fluteMusic = \relative c' { \key g \major g'1 b }
  piccoloMusic = \relative c' { \key g \major g'1 b }
  hautboisMusic = \relative c' { \key g \major g'1 b }
  coranglaisMusic = \transpose c' f
                  \relative c'' { \key bes \major 
                                  bes1 d }
  clarinetMusic = \transpose c' bes
                  \relative c'' { \key bes \major 
                                  bes1 d }
  clarinetBMusic = \transpose c' bes
                  \relative c'' { \key bes \major 
                                  bes1 d }
  bassonMusic = \relative c { \clef bass
                               \key g \major 
                               g'1 b }                  
  % Key signature is often omitted for horns
  hornuntroisMusic = \transpose c' f
                \relative c { d'1 fis }
  horndeuxquatreMusic = \transpose c' f
                \relative c { d'1 fis }
  trumpetMusic = \relative c { g''1 b }
  tromboneundeuxMusic = \relative c { \clef tenor
                                  \key g \major
                                  g'1 b }
  trombonetroisTubaMusic = \relative c { \clef bass 
                                     \key g \major
                                     g'1 b }
  timbalesMusic = \relative c { \clef bass
                                \key g \major g1 b }
  violinIMusic = \relative c' { \key g \major g'1 b }
  violinIIMusic = \relative c' { \key g \major g'1 b }
  violaMusic = \relative c { \clef alto \key g \major g'1 b }
  celloMusic = \relative c { \clef bass \key g \major g1 b }
  bassMusic = \relative c { \clef "bass_8" \key g \major g,1 b }

\score {
    <<
      % bois
      
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new GrandStaff = "flutes" 
        <<
          \new Staff = "Staff_flute" {
            \set Staff.instrumentName = #"Flûtes"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \fluteMusic
          }
          \new Staff = "Staff_piccolo" {
            \set Staff.instrumentName = #"Piccolo"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \piccoloMusic
          }
        >>
        \new GrandStaff = "hautbois" 
        <<
          \new Staff = "Staff_hautbois" {
            \set Staff.instrumentName = #"Hautbois"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \hautboisMusic
          }
          \new Staff = "Staff_CA" {
            \set Staff.instrumentName = #"Cor Anglais"
            \transposition f
            \transpose f c' \coranglaisMusic
          }
        >>
        \new GrandStaff = "clarinettes" 
        <<
          \new Staff = "Staff_clarinet" {
            \set Staff.instrumentName =
              \markup { \concat { "Clarinettes en Si" \flat } }
            % Declare that written Middle C in the music
            % to follow sounds a concert B flat, for
            % output using sounded pitches such as MIDI.
              \transposition bes
            % Print music for a B-flat clarinet
              \transpose bes c' \clarinetMusic
          }
          \new Staff = "Staff_clarinetteB" {
            \set Staff.instrumentName = #"Clarinette Basse"
            \transposition bes
            \transpose f bes' \clarinetBMusic
          }
        >>
        \new Staff = "Staff_basson" {
          \set Staff.instrumentName = #"Bassons"
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \bassonMusic
        }
      >>
      
      % cuivres
      \new StaffGroup = "StaffGroup_brass" <<
        \new GrandStaff = "GrandStaff_cors" \with	 { instrumentName = "Cors en Fa" } 
        <<
          \new Staff = "Staff_horn13" {
            \transposition f
            \transpose f c' \hornuntroisMusic
          }
          \new Staff = "Staff_horn24" {
            \transposition f
            \transpose f c' \horndeuxquatreMusic
          }
        >>
        \new Staff = "Staff_trumpet" {
          \set Staff.instrumentName = #"Trompettes"
          \trumpetMusic
        }
        \new Staff = "Staff_trombones" {
          \set Staff.instrumentName = #"Trombones 1 2"
          \tromboneundeuxMusic
        }
        \new Staff = "Staff_trombonetuba" {
          \set Staff.instrumentName = #"Trombone & Tuba"
          \trombonetroisTubaMusic
        }
      >>    
      
      %percus
      \new Staff = "Staff_timbales" {
        \set Staff.instrumentName = #"Timbales"
        \timbalesMusic
      }
      
      %autres
      
      
      %cordes
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" {
            \set Staff.instrumentName = #"Violons I"
            \violinIMusic
          }
          \new Staff = "Staff_violinII" {
            \set Staff.instrumentName = #"Violons II"
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" {
          \set Staff.instrumentName = #"Altos"
          \violaMusic
        }
        \new Staff = "Staff_cello" {
          \set Staff.instrumentName = #"Violoncelles"
          \celloMusic
        }
        \new Staff = "Staff_bass" {
          \set Staff.instrumentName = #"Contrebasses"
          \bassMusic
        }
      >>
>>
}