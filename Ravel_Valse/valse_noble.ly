\version  "2.18.2"

%#(set-global-staff-size 17)
\paper {
  indent = 3.0\cm % space for instrumentName
  short-indent = 1.5\cm % space for shortInstrumentName
}

  %bois
  fluteMusic = \relative c''' { \key e \minor 
                              \time 3/4 
                              b4^"1." \p \( a \) d, |
                              e \( g \) b |
                              b \( a \) d, |
                              e \( g \) b |
                              e \( d \) e \( |
                              d \) c \( e, \) |
                              a \( b \) d, |
                              e 2. |
                              R2. |
                              r2 <f b>4-- \pp |
                            }
  hautboisMusic = \relative c'' { \key e \minor 
                                 \time 3/4
                                 R2. |
                                 R2. |
                                 R2. |
                                 R2. |
                                 r4 eis2^"1." \p \( |
                                 fis g4 \) |
                                 R2. |
                                 R2. |
                                 R2. |
                                 r2 <b, d>4-- \pp |
  }
  coranglaisMusic = \transpose c' f
                  \relative c'' { \key b \minor 
                                  \time 3/4 
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
                                  R2. |
  }
  clarinetMusic = \transpose c' bes
                  \relative c' { 
                    \key fis \minor
                    \time 3/4
                    <<
                      \new voice \voiceOne
                      {
                        fis8 \( gis  e'4 \) r4 |
                        fis,8  \( a fis'4 \) r4 |
                        fis,8  \( gis e'4 \) r4 |
                        fis,8  \( a fis'4 \) r4 |
                        fis,8 \( ais fis'4 \) fis,8 \( b |
                        fis'4 \) fis,8 \( c' fis4 \) |
                        fis,8  \( gis  e'4 \) r4 |
                        fis,8  \( a fis'4 \) r4 |
                        r2 e4-- |
                        R2. |
                      }
                      \\
                      \new voice \voiceTwo
                      {
                        r4 \pp d4 r4 |
                        r4 e4 r4 |
                        r4 d4 r4 |
                        r4 e4 r4 |
                        r4 e4 r4 |
                        e4 r4 d4 |
                        r4 d4 r4 |
                        r4 e4 r4 |
                        r2 d4-- |
                        R2. |
                      }
                    >>
                  }
  bassonMusic = \relative c' { \clef bass
                               \key e \minor
                                                   \time 3/4
                    <<
                      \new voice \voiceOne
                      {
                        e2 \pp  ~e8 r8 |
                        R2. |
                        e2 ~e8 r8 |
                        R2. |                        
                        e2 ~e8 r8 |
                        R2. |
                        e2 ~e8 r8 |
                        R2. |
                        r2 fis4-- |
                        R2. |
                      }
                      \\
                      \new voice \voiceTwo
                      {
                        R2. |
                        e2 ~e8 r8 |
                        R2. |
                        e2 ~e8 r8 |
                        R2. |
                        e2 ~e8 r8 |
                        R2. |
                        e2 ~e8 r8 |
                        r2 d4-- |
                        R2. |
                      }
                    >>
  }                  
  % Key signature is often omitted for horns
  hornuntroisMusic = \transpose c' f
                \relative c { R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                }
  horndeuxquatreMusic = \transpose c' f
                \relative c { R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                R2. |
                R2. |
                }
  trumpetMusic = \relative c { R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                R2. |
                R2. |
  }
  tromboneundeuxMusic = \relative c { \clef tenor
                                  \key e \minor
                                  R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                R2. |
                R2. |
  }
  trombonetroisTubaMusic = \relative c { \clef bass 
                                     \key e \minor
                                     R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                R2. |
                R2. |
  }
  timbalesMusic = \relative c { \clef bass
                                \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                R2. |
                R2. |
  }
  
  harpeMusicMD =\relative c''
                   { 
                     \key e \minor
                     \time 3/4
                     r4 \p r4 <c d> |
                     r4 r4 <d e> |
                     r4 r4 <c d> |
                     r4 r4 <d e> |
                     r4 <d e> r4 |
                     <d e> r4 <c e> |
                     r4 r4 <c d> |
                     r4 r4 <d e> |
                     r4 r4 <d, fis c' d>4 \arpeggio |
                     r4 r4 <b' d fis b>4 \arpeggio ||
                   }
  
  
  harpeMusicMG =   \relative c'
                   { 
                     \clef bass
                     \key e \minor
                     \time 3/4
                     e4 r4 r4 |
                     e4 r4 r4 |
                     e4 r4 r4 |
                     e4 r4 r4 |
                     e4 r4 e4 |
                     r4 e4 r4 |
                     e4 r4 r4 |
                     e4 r4 r4 |
                     e,4 r4 r4 |
                     e4 r4 r4 |
                   }
  
  violinIMusic = \relative c' { \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                b''4^\markup{ \italic "avec sourdine"} \p \( a \) d, |
                e \( g \) b |
  }
  violinIIMusic = \relative c' { \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                b'4^\markup{ \italic "avec sourdine"} \p \( a \) d, |
                e \( g \) b |
  }
  violaMusic = \relative c { \clef alto \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                e8 \p \( c' fis4 \) r4 |
                e,8 \( b' g'4 \) r |
  }
  celloMusic = \relative c { \clef bass \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. | 
                e2 \p ~e8 r8 |
                e2 ~e8 r8 |
  }
  bassMusic = \relative c { \clef "bass_8" \key e \minor R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
                R2. |
  }

\score {
    <<
      % bois
      
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new GrandStaff = "flutes" 
        <<
          \new Staff = "Staff_flute" {
            \set Staff.instrumentName = #"Flûtes"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \fluteMusic
          }
        >>
        \new GrandStaff = "hautbois" 
        <<
          \new Staff = "Staff_hautbois" {
            \set Staff.instrumentName = #"Hautbois"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \hautboisMusic
          }
          \new Staff = "Staff_CA" {
            \set Staff.instrumentName = #"Cor Anglais"
            \transposition f
            \transpose f c' \coranglaisMusic
          }
        >>
        \new GrandStaff = "clarinettes" 
        <<
          \new Staff = "Staff_clarinet" {
            \set Staff.instrumentName =
              \markup { \concat { "Clarinettes en Si" \flat } }
            % Declare that written Middle C in the music
            % to follow sounds a concert B flat, for
            % output using sounded pitches such as MIDI.
              \transposition bes
            % Print music for a B-flat clarinet
              \transpose bes c' \clarinetMusic
          }
        >>
        \new Staff = "Staff_basson" {
          \set Staff.instrumentName = #"Bassons"
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \bassonMusic
        }
      >>
      
      % cuivres
      \new StaffGroup = "StaffGroup_brass" <<
        \new GrandStaff = "GrandStaff_cors" \with	 { instrumentName = "Cors en Fa" } 
        <<
          \new Staff = "Staff_horn13" {
            \transposition f
            \transpose f c' \hornuntroisMusic
          }
          \new Staff = "Staff_horn24" {
            \transposition f
            \transpose f c' \horndeuxquatreMusic
          }
        >>
        \new Staff = "Staff_trumpet" {
          \set Staff.instrumentName = #"Trompettes"
          \trumpetMusic
        }
        \new Staff = "Staff_trombones" {
          \set Staff.instrumentName = #"Trombones 1 2"
          \tromboneundeuxMusic
        }
        \new Staff = "Staff_trombonetuba" {
          \set Staff.instrumentName = #"Trombone & Tuba"
          \trombonetroisTubaMusic
        }
      >>    
      
      %percus
      \new Staff = "Staff_timbales" {
        \set Staff.instrumentName = #"Timbales"
        \timbalesMusic
      }
      
      %autres
            \new PianoStaff = "Staff_harp" \with{
          instrumentName = #"Harpe"
        } 
        <<
          \set PianoStaff.connectArpeggios = ##t
          \new Staff = "RH"
          <<
            \harpeMusicMD
          >>
          \new Staff = "LH"
          <<
            \harpeMusicMG
          >>
        >>
      
      %cordes
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" {
            \set Staff.instrumentName = #"Violons I"
            \violinIMusic
          }
          \new Staff = "Staff_violinII" {
            \set Staff.instrumentName = #"Violons II"
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" {
          \set Staff.instrumentName = #"Altos"
          \violaMusic
        }
        \new Staff = "Staff_cello" {
          \set Staff.instrumentName = #"Violoncelles"
          \celloMusic
        }
        \new Staff = "Staff_bass" {
          \set Staff.instrumentName = #"Contrebasses"
          \bassMusic
        }
      >>
>>
}