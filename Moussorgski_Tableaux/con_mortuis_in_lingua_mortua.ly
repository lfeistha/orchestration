\version  "2.18.2"

%#(set-global-staff-size 17)
\paper {
  indent = 3.0\cm % space for instrumentName
  short-indent = 1.5\cm % space for shortInstrumentName
}

  %bois
  fluteMusic = \relative c' { \key b \minor 
    R1. |
    R1. |
    R1. |
    <<
      \new Voice \voiceOne {
        r4 fis''2 \pp eis4 e d |
        b ais a gis g2 |
        eis2 fis4
      }
    \\
      \new Voice \voiceTwo { 
        r4 fis2 eis4 e d |
        b ais a gis g2 |
        eis2 fis4
      }
    >>
    r4 r4 r4 |
    R1. |
    R1. |
    r4 
    <<
      \new Voice \voiceOne {
        e''2 cis4 e2 |
        cis4 c b ais a gis |
        g eis2 fis2. |
      }
      \\
      \new Voice \voiceTwo {
        e2 cis4 e2 |
        cis4 c b ais a gis |
        g eis2 fis2. |
      }
    >>
  }
  piccoloMusic = \relative c' { \key b \minor
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
  }
  hautboisMusic = \relative c'' { \key b \minor
    R1. |
    r4 g^"1." \p \( fis b cis fis \) |
    d \( cis fis d cis b  |
    fis \) r4 r4 r2. |
    R1. |
    R1. |
    R1. |
    r2 <d'fis>4 \( d^"à 2" cis b |
    fis \) r4 r4 r2. |
    R1. |
    R1. |
  }
  coranglaisMusic = \transpose c' f
                  \relative c'' { \key fis \minor
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    r2 e4 \pp \( fis cis d |
    cis \) r4 r4 r2. |
    R1. |
    R1. |
  }
  clarinetMusic = \transpose c' a
                  \relative c'' { \key d \minor 
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    r2 r2
    <<
      \new Voice \voiceOne {
        c4 \( d \) |
        a1 r2 |
      }
      \\
      \new Voice \voiceTwo {
        a4 \( bes \) |
        e,2 r2 r2 |
      }
    >>
    R1. |
    R1. |
    R1. |
  }
  clarinetBMusic = \transpose c' a
                  \relative c'' { \key d \minor 
    R1. |
    R1. |
    R1. |
    r4 bes^"à 2" \( \pp a  d  e  a \) |
    f \( e  a f2 d4 \) |
    e \( d a \) r4 r4 r4 |
    r2 r2 f''2 |
    cis4 a r2 r2  |
    r4 a, \( bes g \) a \( bes |
    g \) c \( d a2 a'4 \) |
    f \( e d a \) r4 r4 |                             
  }
  bassonMusic = \relative c { \clef bass
                               \key b \minor
    R1. |
    R1. |
    R1. |
    r4 g^"à 2" \( \pp fis  b  cis  fis \) |
    d \( cis  fis d2 b4 \) |
    cis \( b fis \) r4 r4 r4 |
    R1. |
    R1. |
    r4 fis \( g e \) fis \( g |
    e \) a \( b fis2 fis'4 \) |
    d \( cis b fis \) r4 r4 |                             
  }    
  cbassonMusic = \relative c { \clef bass
                               \key b \minor
    R1. |
    R1. |
    R1. |
    r4 g \( \pp fis  b  cis  fis \) |
    d \( cis  fis d2 b4 \) |
    cis \( b fis \) r4 r4 r4 |
    R1. |
    R1. |
    r4 fis \( g e \) fis \( g |
    e \) a \( b fis2 fis'4 \) |
    d \( cis b fis \) r4 r4 |  
  }
  % Key signature is often omitted for horns
  hornuntroisMusic = \transpose c' f
                \relative c { 
    R1. |
    r4 
    <<
      \new Voice \voiceOne {
        b'' gis cis eis4. r8 |
        fis4 fis eis8 r8 r2. |
      }
      \\
      \new Voice \voiceTwo {
        fis,4 eis a cis4. r8 |
        d4 bis cis8 r8 r2. |
      }
    >>
    R1. |
    R1. |
    r2 r2  
    <<
      \new Voice \voiceOne {
        cis4 \( d |
        b \)
      }
      \\
      \new Voice \voiceTwo {
        eis,4 \( fis |
        d \)
      }
    >>
    r4 r2 r2 |
    R1.|
    R1.|
    R1.|
    R1.|
                }
  horndeuxquatreMusic = \transpose c' f
                \relative c { 
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    r2 r2 gis''4^"3." \( a |
    g! \) r4 r2 r2 |
    R1. |
    R1. |
    R1. |
    R1. |
                }
  trumpetMusic = \relative c { 
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
  }
  tromboneundeuxMusic = \relative c { \clef tenor
                                  \key b \minor
    R1. |
    R1.|
    R1.|
    R1.|
    R1.|
    R1.|
    r4
    <<
      \new Voice \voiceOne {
        fis'4 \( g e \) 
      }
      \\
      \new Voice \voiceTwo {
        ais,4 \( b cis \)
      }
    >>   
    r2 |
    R1.|
    R1.|
    R1.|
    R1.|
  }
  trombonetroisTubaMusic = \relative c { \clef bass 
                                     \key b \minor
    R1. |
    R1. |
    R1. |
  <<
    \new Voice \voiceOne {
      R1. |
      R1. |
      R1. |
      r4 fis\( g a \)r2 |
      R1. |
      R1. |
      R1. |
      R1. |
    }
    \\
    \new Voice \voiceTwo {
      r4 g, \( \pp fis  b  cis  fis \) |
      d \( cis  fis d2 b4 \) |
      cis \( b fis \) r4 r4 r4 |
      R1. |
      R1. |
      r4 fis \( g e \) fis \( g |
      e \) a \( b fis2 fis'4 \) |
      d \( cis b fis \) r4 r4 | 
    }
  >>
  }
  timbalesMusic = \relative c { \clef bass
                                \key b \minor 
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
    R1. |
  }
  violinIMusic = \relative c''' { \key b \minor 
    \repeat tremolo 48 fis32^"Avec sourdine" \pp |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 8 eis32 \repeat tremolo 8 e32  \repeat tremolo 8 d32 |
    \repeat tremolo 8 b32  \repeat tremolo 8 ais32  \repeat tremolo 8 a32  \repeat tremolo 8 gis32  \repeat tremolo 16 g32 |
    \repeat tremolo 16 eis32 \repeat tremolo 8 fis32 \repeat tremolo 8 fis'32 \repeat tremolo 8 fis32 \repeat tremolo 8 f32 |
    \repeat tremolo 8 e32 \repeat tremolo 8 cis32 \repeat tremolo 8 b32 \repeat tremolo 8 a32 \repeat tremolo 16 d32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 fis32 \repeat tremolo 8 a!32 \repeat tremolo 24 fis32 |
    \repeat tremolo 8 fis32 \repeat tremolo 16 e32 \repeat tremolo 8 cis32 \repeat tremolo 16 e32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 c32 \repeat tremolo 8 b32 \repeat tremolo 8 ais32 \repeat tremolo 8 a32 \repeat tremolo 8 gis32 |
    \repeat tremolo 8 g32 \repeat tremolo 16 eis32 \repeat tremolo 24 fis32 | 
  }
  violinIIMusic = \relative c''' { \key b \minor 
      \repeat tremolo 48 fis32^"Avec sourdine" \pp |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 8 eis32 \repeat tremolo 8 e32  \repeat tremolo 8 d32 |
    \repeat tremolo 8 b32  \repeat tremolo 8 ais32  \repeat tremolo 8 a32  \repeat tremolo 8 gis32  \repeat tremolo 16 g32 |
    \repeat tremolo 16 eis32 \repeat tremolo 8 fis32 \repeat tremolo 8 fis'32 \repeat tremolo 8 fis32 \repeat tremolo 8 f32 |
    \repeat tremolo 8 e32 \repeat tremolo 8 cis32 \repeat tremolo 8 b32 \repeat tremolo 8 a32 \repeat tremolo 16 d32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 fis32 \repeat tremolo 8 a!32 \repeat tremolo 24 fis32 |
    \repeat tremolo 8 fis32 \repeat tremolo 16 e32 \repeat tremolo 8 cis32 \repeat tremolo 16 e32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 c32 \repeat tremolo 8 b32 \repeat tremolo 8 ais32 \repeat tremolo 8 a32 \repeat tremolo 8 gis32 |
    \repeat tremolo 8 g32 \repeat tremolo 16 eis32 \repeat tremolo 24 fis32 | 
  }
  violaMusic = \relative c'' { \clef alto \key b \minor 
      \repeat tremolo 48 fis32^"Avec sourdine" \pp |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 24 fis32  |
    \repeat tremolo 24 fis \repeat tremolo 8 eis32 \repeat tremolo 8 e32  \repeat tremolo 8 d32 |
    \repeat tremolo 8 b32  \repeat tremolo 8 ais32  \repeat tremolo 8 a32  \repeat tremolo 8 gis32  \repeat tremolo 16 g32 |
    \repeat tremolo 16 eis32 \repeat tremolo 8 fis32 \repeat tremolo 8 fis'32 \repeat tremolo 8 fis32 \repeat tremolo 8 f32 |
    \repeat tremolo 8 e32 \repeat tremolo 8 cis32 \repeat tremolo 8 b32 \repeat tremolo 8 a32 \repeat tremolo 16 d32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 fis32 \repeat tremolo 8 a!32 \repeat tremolo 24 fis32 |
    \repeat tremolo 8 fis32 \repeat tremolo 16 e32 \repeat tremolo 8 cis32 \repeat tremolo 16 e32 |
    \repeat tremolo 8 cis32 \repeat tremolo 8 c32 \repeat tremolo 8 b32 \repeat tremolo 8 ais32 \repeat tremolo 8 a32 \repeat tremolo 8 gis32 |
    \repeat tremolo 8 g32 \repeat tremolo 16 eis32 \repeat tremolo 24 fis32 | 
  }
  celloMusic = \relative c { \clef bass \key b \minor 
    R1. |
    R1. |
    R1. |
    r4 g \( \pp fis \) b \( cis \) fis |
    d \downbow \( cis \) fis d2 b4 |
    cis \( b fis \) r4 r4 r4 |
    R1. |
    R1. |
    r4 fis \( g e \) fis \( g |
    e \) a \( b \) fis2 fis'4 |
    d \( cis \) b fis r4 r4 |
  }
  bassMusic = \relative c { \clef "bass_8" \key b \minor 
    R1. |
    R1. |
    R1. |
    r4 g, \( \pp fis \) b \( cis \) fis |
    d \downbow \( cis \) fis d2 b4 |
    cis \( b fis \) r4 r4 r4 |
    R1. |
    R1. |
    r4 fis \( g e \) fis \( g |
    e \) a \( b \) fis2 fis'4 |
    d \( cis \) b fis r4 r4 |
  }

\score {
    <<
      % bois
      \time 6/4
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new GrandStaff = "flutes" 
        <<
          \new Staff = "Staff_flute" {
            \set Staff.instrumentName = #"Flûtes"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \fluteMusic
          }
          \new Staff = "Staff_piccolo" {
            \set Staff.instrumentName = #"Piccolo"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \piccoloMusic
          }
        >>
        \new GrandStaff = "hautbois" 
        <<
          \new Staff = "Staff_hautbois" {
            \set Staff.instrumentName = #"Hautbois"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \hautboisMusic
          }
          \new Staff = "Staff_CA" {
            \set Staff.instrumentName = #"Cor Anglais"
            \transposition f
            \transpose f c' \coranglaisMusic
          }
        >>
        \new GrandStaff = "clarinettes" 
        <<
          \new Staff = "Staff_clarinet" {
            \set Staff.instrumentName =
              \markup { \concat { "Clarinettes en A"} }
            % Declare that written Middle C in the music
            % to follow sounds a concert B flat, for
            % output using sounded pitches such as MIDI.
              \transposition a
            % Print music for a B-flat clarinet
              \transpose a c' \clarinetMusic
          }
          \new Staff = "Staff_clarinetteB" {
            \set Staff.instrumentName = #"Clarinette Basse"
            \transposition a
            \transpose a c \clarinetBMusic
          }
        >>
        \new GrandStaff = "bassons" {
        <<  
          \new Staff = "Staff_basson" {
            \set Staff.instrumentName = #"Bassons"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \bassonMusic
          }
          \new Staff = "Staff_cbasson" {
            \set Staff.instrumentName = #"Contre-Basson"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \cbassonMusic
          }
        >>
        }
      >>
      
      % cuivres
      \new StaffGroup = "StaffGroup_brass" <<
        \new GrandStaff = "GrandStaff_cors" \with	 { instrumentName = "Cors en Fa" } 
        <<
          \new Staff = "Staff_horn13" {
            \transposition f
            \transpose f c' \hornuntroisMusic
          }
          \new Staff = "Staff_horn24" {
            \transposition f
            \transpose f c' \horndeuxquatreMusic
          }
        >>
        \new Staff = "Staff_trumpet" {
          \set Staff.instrumentName = #"Trompettes"
          \trumpetMusic
        }
        \new Staff = "Staff_trombones" {
          \set Staff.instrumentName = #"Trombones 1 2"
          \tromboneundeuxMusic
        }
        \new Staff = "Staff_trombonetuba" {
          \set Staff.instrumentName = #"Trombone & Tuba"
          \trombonetroisTubaMusic
        }
      >>    
      
      %percus
      \new Staff = "Staff_timbales" {
        \set Staff.instrumentName = #"Timbales"
        \timbalesMusic
      }
      
      %autres
      
      
      %cordes
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" {
            \set Staff.instrumentName = #"Violons I"
            \violinIMusic
          }
          \new Staff = "Staff_violinII" {
            \set Staff.instrumentName = #"Violons II"
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" {
          \set Staff.instrumentName = #"Altos"
          \violaMusic
        }
        \new Staff = "Staff_cello" {
          \set Staff.instrumentName = #"Violoncelles"
          \celloMusic
        }
        \new Staff = "Staff_bass" {
          \set Staff.instrumentName = #"Contrebasses"
          \bassMusic
        }
      >>
>>
}