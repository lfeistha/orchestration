\version  "2.18.2"

%#(set-global-staff-size 17)
\paper {
  indent = 3.0\cm % space for instrumentName
  short-indent = 1.5\cm % space for shortInstrumentName
}

  %bois
  fluteMusic = \relative c'' { <c c'>2 <e e'>2 | <d d'> <c c'> <a' c> <c e> <b d> <a c> }
  piccoloMusic = \relative c'' { c2 e d c c2 e d c }
  hautboisMusic = \relative c'' {<e g> <g b> <f a> <e g>  <a, f'> <c a'> <b g'> <a f'>}
  coranglaisMusic = \transpose c' f
                  \relative c'' { \key g \major d fis e d c e d c  }
  clarinetMusic = \transpose c' bes
                  \relative c' { \key d \major
                                  <fis d'>2 <a fis'> <g e'> <fis d'>  <d d'> <fis fis'> <e e'> <d d'> }
                                 
  clarinetBMusic = \transpose c' bes
                  \relative c' { \key d \major 
                                  d f e d g b a g }
  bassonMusic = \relative c { \clef bass
                               c^"à 2"  e  d  c <f, f'> <a a'> <g g'> <f f'>}                  
  % Key signature is often omitted for horns
  hornuntroisMusic = \transpose c' f
                \relative c'' { <g d'> <fis d'> <e c'> <d b'> <g e'> <g e'> <a fis'> <g e'> }
  horndeuxquatreMusic = \transpose c' f
                \relative c' { <d b'> <d b'> <c a'> <b g'> <e c'> <e b'> <d d'> <e c'> }
  trumpetMusic = \relative c'' { c^"à 2" b a g c e d c }
  tromboneundeuxMusic = \relative c { \clef tenor
                                  c'^"à 2" b a g <a f'> <c a'> <b g'> <a f'> }
  trombonetroisTubaMusic = \relative c { \clef bass 
                                     c^"à 2" b a g <f f'> <a a'> <g g'> <f f'>}

  violinIMusic = \relative c'' { c e d c f e d c }
  violinIIMusic = \relative c'' { c e d c f e d c }
  violaMusic = \relative c { \clef alto c' e d c f e d c }
  celloMusic = \relative c { \clef bass c e d c f a g f}
  bassMusic = \relative c { \clef "bass_8" c, e d c f a g f  }

\score {
    <<
      % bois
      
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new GrandStaff = "flutes" 
        <<
          \new Staff = "Staff_flute" {
            \set Staff.instrumentName = #"Flûtes"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \fluteMusic
          }
          \new Staff = "Staff_piccolo" {
            \set Staff.instrumentName = #"Piccolo"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \piccoloMusic
          }
        >>
        \new GrandStaff = "hautbois" 
        <<
          \new Staff = "Staff_hautbois" {
            \set Staff.instrumentName = #"Hautbois"
            % shortInstrumentName, midiInstrument, etc.
            % may be set here as well
            \hautboisMusic
          }
          \new Staff = "Staff_CA" {
            \set Staff.instrumentName = #"Cor Anglais"
            \transposition f
            \transpose f c' \coranglaisMusic
          }
        >>
        \new GrandStaff = "clarinettes" 
        <<
          \new Staff = "Staff_clarinet" {
            \set Staff.instrumentName =
              \markup { \concat { "Clarinettes en Si" \flat } }
            % Declare that written Middle C in the music
            % to follow sounds a concert B flat, for
            % output using sounded pitches such as MIDI.
              \transposition bes
            % Print music for a B-flat clarinet
              \transpose bes c' \clarinetMusic
          }
          \new Staff = "Staff_clarinetteB" {
            \set Staff.instrumentName = #"Clarinette Basse"
            \transposition bes
            \transpose bes c' \clarinetBMusic
          }
        >>
        \new Staff = "Staff_basson" {
          \set Staff.instrumentName = #"Bassons"
          % shortInstrumentName, midiInstrument, etc.
          % may be set here as well
          \bassonMusic
        }
      >>
      
      % cuivres
      \new StaffGroup = "StaffGroup_brass" <<
        \new GrandStaff = "GrandStaff_cors" \with	 { instrumentName = "Cors en Fa" } 
        <<
          \new Staff = "Staff_horn13" {
            \transposition f
            \transpose f c' \hornuntroisMusic
          }
          \new Staff = "Staff_horn24" {
            \transposition f
            \transpose f c' \horndeuxquatreMusic
          }
        >>
        \new Staff = "Staff_trumpet" {
          \set Staff.instrumentName = #"Trompettes"
          \trumpetMusic
        }
        \new Staff = "Staff_trombones" {
          \set Staff.instrumentName = #"Trombones 1 2"
          \tromboneundeuxMusic
        }
        \new Staff = "Staff_trombonetuba" {
          \set Staff.instrumentName = #"Trombone & Tuba"
          \trombonetroisTubaMusic
        }
      >>    
      
      %percus
      %
      %autres
      
      
      %cordes
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" {
            \set Staff.instrumentName = #"Violons I"
            \violinIMusic
          }
          \new Staff = "Staff_violinII" {
            \set Staff.instrumentName = #"Violons II"
            \violinIIMusic
          }
        >>
        \new Staff = "Staff_viola" {
          \set Staff.instrumentName = #"Altos"
          \violaMusic
        }
        \new Staff = "Staff_cello" {
          \set Staff.instrumentName = #"Violoncelles"
          \celloMusic
        }
        \new Staff = "Staff_bass" {
          \set Staff.instrumentName = #"Contrebasses"
          \bassMusic
        }
      >>
>>
}